CREATE TABLE machine (
  mid SERIAL PRIMARY KEY NOT NULL,
  uri TEXT NOT NULL,
  mname TEXT NULL,
  active BOOLEAN NULL,
  created TIMESTAMPTZ NOT NULL,
  location TEXT NULL,
  description TEXT NULL,
  changed BOOLEAN NOT NULL
);

CREATE TABLE sensor (
  sid SERIAL PRIMARY KEY NOT NULL,
  mid INTEGER NOT NULL,
  mname TEXT NULL,
  namespace INTEGER NOT NULL,
  nodeid INTEGER NOT NULL,
  sname TEXT NULL,
  valuetype TEXT NOT NULL,
  valuemindelta REAL NOT NULL,
  unit TEXT NOT NULL,
  active BOOLEAN NOT NULL,
  created TIMESTAMPTZ NOT NULL,
  target TEXT NULL,
  changed BOOLEAN NOT NULL
);

CREATE TABLE machineData (
  mid INTEGER NOT NULL,
  connected BOOLEAN NOT NULL,
  ts TIMESTAMPTZ NOT NULL
);

CREATE TABLE sensorData (
  sid INTEGER NOT NULL,
  value TEXT NOT NULL,
  ts TIMESTAMPTZ NOT NULL
);

CREATE TABLE dataTarget (
  did SERIAL PRIMARY KEY NOT NULL,
  type TEXT NULL,
  destination TEXT NULL,
  username TEXT NULL,
  password TEXT NULL,
  connected BOOLEAN NULL,
  description TEXT NULL,
  changed BOOLEAN NOT NULL
);

CREATE TABLE rule (
  rid INTEGER NOT NULL,
  subrid INTEGER NOT NULL,
  rname TEXT NULL,
  parameter TEXT NOT NULL,
  operation TEXT NOT NULL,
  sensorused TEXT NOT NULL
);

CREATE TABLE aggregate (
  aid INTEGER NOT NULL,
  subaid INTEGER NOT NULL,
  aname TEXT NULL,
  parameter TEXT NOT NULL,
  operation TEXT NOT NULL,
  sensorused TEXT NOT NULL
);

CREATE TABLE event (
  eventid SERIAL PRIMARY KEY NOT NULL,
  rid INTEGER NOT NULL,
  eventtype TEXT NOT NULL,
  message TEXT NOT NULL,
  triggervalue BOOLEAN NOT NULL
);

