package locales;


public class MessagesEN {

    //Database
    public static final String DATABASE_CONNECTED_1 = "Successfully connected to database <";
    public static final String DATABASE_CONNECTED_2 = "> on ";
    public static final String DATABASE_CONNECTED_3 = " ! :)";

    //Sensor verticles
    public static final String SENSOR_DEPLOYED_1 = "New sensor verticle with deployment ID <";
    public static final String SENSOR_DEPLOYED_2 = "> deployed!";
    public static final String SENSOR_UNDEPLOYED_1 = "Sensor verticle with deployment ID <";
    public static final String SENSOR_UNDEPLOYED_2 = "> successfully undeployed!";

    //Machines
    public static final String MACHINE_DEACTIVATED_1 = "Machine with ID: <";
    public static final String MACHINE_DEACTIVATED_2 = "> successfully deactivated!";


}
