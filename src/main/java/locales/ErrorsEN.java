package locales;


public class ErrorsEN {

    //Restart errors
    public static final String RESTART_M_AND_S_NOT_LOADABLE = "Machines and sensors not loadable because there is no active connection to the database!";
    public static final String RESTART_S_HAS_NO_KNOWN_M = "";

    //Database errors
    public static final String DATABASE_CONNECT_FIRST = "You are not connected to a database. Please use the function connect() to connect to a database first!";
    public static final String DATABASE_CONNECTION_FAILED = "Cannot connect to the database. Please check the configs in the database.properties file!";

    //Sensor errors
    public static final String SENSOR_ALREADY_ACTIVE_1 = "Cannot add sensor with ID: ";
    public static final String SENSOR_ALREADY_ACTIVE_2 = " because it is already an active sensor!";
    public static final String SENSOR_DEPLOY_FAILED_1 = "Deploying sensor verticle with sensor ID <";
    public static final String SENSOR_DEPLOY_FAILED_2 = "> failed!";
    public static final String SENSOR_UNDEPLOY_FAILED_1 = "Undeploying sensor verticle with deployment ID <";
    public static final String SENSOR_UNDEPLOY_FAILED_2 = "> failed!";
    public static final String SENSOR_ADD_FAILED_ALREADY_EXISTS_1 = "Sensor with NodeID <";
    public static final String SENSOR_ADD_FAILED_ALREADY_EXISTS_2 = "> and Namespace <";
    public static final String SENSOR_ADD_FAILED_ALREADY_EXISTS_3 = "> on machine with ID <";
    public static final String SENSOR_ADD_FAILED_ALREADY_EXISTS_4 = "> is already active!";

    //Frontend sensor errors
    public static final String FRONTEND_SENSOR_ADD_FAILED = "There was an error while adding the new sensor to the backend!";
    public static final String FRONTEND_SENSORS_NOT_FOUND = "Can't find any active sensors!";
    public static final String FRONTEND_SENSOR_DELETE_NOT_FOUND_1 = "Can't delete sensor with ID: ";
    public static final String FRONTEND_SENSOR_DELETE_NOT_FOUND_2 = " because its not an active sensor!";
    public static final String FRONTEND_MACHINES_NOT_FOUND = "Can't find any known machines!";

}
