package configUtils;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.util.Properties;


public class ServerCfgReader {

    public static int getServerPort() throws Exception {
        Properties p = new Properties();
        BufferedInputStream b = new BufferedInputStream(new FileInputStream("./config/server.properties"));
        p.load(b);
        b.close();
        String prop = p.getProperty("serverport");
        int port = Integer.parseInt(prop);
        return port;
    }

    public static long getPeriodicTime() throws Exception {
        Properties p = new Properties();
        BufferedInputStream b = new BufferedInputStream(new FileInputStream("./config/server.properties"));
        p.load(b);
        b.close();
        String prop = p.getProperty("msRefresh");
        long periodic = Integer.parseInt(prop);
        return periodic;
    }

}
