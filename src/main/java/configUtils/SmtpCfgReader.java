package configUtils;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.util.Properties;


public class SmtpCfgReader {

    public static String getConfig(String configParam) throws Exception {
        Properties p = new Properties();
        BufferedInputStream b = new BufferedInputStream(new FileInputStream("./config/smtp.properties"));
        p.load(b);
        b.close();
        return p.getProperty(configParam);
    }

}
