package configUtils;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.util.Properties;


public class DatabaseCfgReader {

    public static String getUrl() throws Exception {
        Properties p = new Properties();
        BufferedInputStream b = new BufferedInputStream(new FileInputStream("./config/database.properties"));
        p.load(b);
        b.close();
        String url = "jdbc:postgresql://"+getHostname()+":"+getPort()+"/"+getDatabase();
        return url;
    }

    public static String getHostname() throws Exception {
        Properties p = new Properties();
        BufferedInputStream b = new BufferedInputStream(new FileInputStream("./config/database.properties"));
        p.load(b);
        b.close();
        return p.getProperty("hostname");
    }

    public static String getPort() throws Exception {
        Properties p = new Properties();
        BufferedInputStream b = new BufferedInputStream(new FileInputStream("./config/database.properties"));
        p.load(b);
        b.close();
        return p.getProperty("port");
    }

    public static String getUsername() throws Exception {
        Properties p = new Properties();
        BufferedInputStream b = new BufferedInputStream(new FileInputStream("./config/database.properties"));
        p.load(b);
        b.close();
        return p.getProperty("username");
    }

    public static String getPassword() throws Exception {
        Properties p = new Properties();
        BufferedInputStream b = new BufferedInputStream(new FileInputStream("./config/database.properties"));
        p.load(b);
        b.close();
        return p.getProperty("password");
    }

    public static String getDatabase() throws Exception {
        Properties p = new Properties();
        BufferedInputStream b = new BufferedInputStream(new FileInputStream("./config/database.properties"));
        p.load(b);
        b.close();
        return p.getProperty("database");
    }

}
