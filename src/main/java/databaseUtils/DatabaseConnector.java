package databaseUtils;

import configUtils.DatabaseCfgReader;
import constants.DatabaseConstants;
import interfaces.DatabaseReaderInterface;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import jsonUtils.JsonParser;
import locales.ErrorsEN;
import locales.MessagesEN;
import ruleSystem.*;
import serverUtils.Machine;
import serverUtils.MachineSet;
import serverUtils.Sensor;
import serverUtils.SensorSet;

import javax.xml.transform.Result;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;


public class DatabaseConnector implements DatabaseReaderInterface {

    //Local variables
    private String hostname;
    private String port;
    private String username;
    private String password;
    private String database;
    //Local database variables
    private Connection c;

    public String connect() throws Exception {
        this.hostname = DatabaseCfgReader.getHostname();
        this.port = DatabaseCfgReader.getPort();
        this.username = DatabaseCfgReader.getUsername();
        this.password = DatabaseCfgReader.getPassword();
        this.database = DatabaseCfgReader.getDatabase();

        Class.forName("org.postgresql.Driver");
        c = DriverManager.getConnection("jdbc:postgresql://"+hostname+":"+port+"/"+database,username,password);
        if(c.isValid(DatabaseConstants.DATABASE_TIMEOUT)) {
            return MessagesEN.DATABASE_CONNECTED_1+database+MessagesEN.DATABASE_CONNECTED_2+hostname+":"+port+MessagesEN.DATABASE_CONNECTED_3;
        }
        return ErrorsEN.DATABASE_CONNECTION_FAILED;
    }

    public void close() throws Exception {
        c.close();
    }

    public boolean isConnected() throws Exception {
        if(c.isValid(DatabaseConstants.DATABASE_TIMEOUT)) {
            return true;
        }
        return false;
    }

    public String getSensorValue(int sensorID) throws Exception {
        if (c.isValid(DatabaseConstants.DATABASE_TIMEOUT)) {
            String query = "SELECT value, ts FROM sensordata WHERE sid = " + sensorID + " ORDER BY ts DESC LIMIT 1;";
            Statement s = c.createStatement();
            ResultSet rs = s.executeQuery(query);
            if(rs.next()) {
                return rs.getString("value");
            } else {
                return "undefinied";
            }
        } else {
            throw new Exception(ErrorsEN.DATABASE_CONNECT_FIRST);
        }
    }

    public double getSensorMed(int sensorID, String period) throws Exception{
        double value = 0;
        ArrayList<Double> valueList = new ArrayList<>();
        int p = Integer.parseInt(period);
        if (c.isValid(DatabaseConstants.DATABASE_TIMEOUT)) {
            String query = "SELECT value FROM  (select * from "+database+".public.sensordata where ts >= NOW() - interval '"+period+" second'  and ts <= NOW()) as val WHERE sid ="+ sensorID+"ORDER BY value ASC";
            Statement s = c.createStatement();
            ResultSet rs = s.executeQuery(query);
            while(rs.next()) {
                value = rs.getDouble("value");
                valueList.add(value);
            }

            int size = valueList.size();
            if (size == 0){
                return 0.0;
            }
            if(size%2 == 0){
                value = valueList.get(size/2);
            }else{
                double valueFirst = valueList.get((int)size/2);
                double valueSecond = valueList.get((int) (size/2)+1);
                value = (valueFirst+valueSecond)/2;
            }

        } else {
            throw new Exception(ErrorsEN.DATABASE_CONNECT_FIRST);
        }

        return value;
    }

    public double getSensorAvg(int sensorID, String period) throws Exception{
        double value = 0;
        ArrayList<Double> valueList = new ArrayList<>();
        int p = Integer.parseInt(period);
        if (c.isValid(DatabaseConstants.DATABASE_TIMEOUT)) {
            String query = "SELECT AVG(value) FROM  (select * from "+database+".public.sensordata where ts >= NOW() - interval '" + period + " second'  and ts <= NOW()) as val WHERE sid =" + sensorID;
            Statement s = c.createStatement();
            ResultSet rs = s.executeQuery(query);
            while (rs.next()) {

                value = rs.getDouble("avg");
                valueList.add(value);

            }
            if (valueList.size() == 0) {
                return 0.0;
            }

        }else {
            throw new Exception(ErrorsEN.DATABASE_CONNECT_FIRST);
        }
        return value;

    }

    public double getSensorMax(int sensorID, String period) throws Exception{
        double value = 0;
        ArrayList<Double> valueList = new ArrayList<>();
        period = period.replace('(',' ');
        period = period.replace(')',' ');
        period = period.trim();
        int p = Integer.parseInt(period);
        if (c.isValid(DatabaseConstants.DATABASE_TIMEOUT)) {
            String query = "SELECT max(value) FROM  (select * from "+database+".public.sensordata where ts >= NOW() - interval '"+period+" second'  and ts <= NOW()) as val WHERE sid ="+ sensorID ;
            Statement s = c.createStatement();
            ResultSet rs = s.executeQuery(query);
            while(rs.next()) {

                value = rs.getDouble("max");
                valueList.add(value);

            }

            if (valueList.size() == 0) {
                return 0.0;
            }
        } else {
            throw new Exception(ErrorsEN.DATABASE_CONNECT_FIRST);
        }
        return value;
    }

    public double getSensorMin(int sensorID, String period) throws Exception{
        double value = 0;
        ArrayList<Double> valueList = new ArrayList<>();
        period = period.replace('(',' ');
        period = period.replace(')',' ');
        period = period.trim();
        int p = Integer.parseInt(period);
        if (c.isValid(DatabaseConstants.DATABASE_TIMEOUT)) {
            String query = "SELECT min(value) FROM  (select * from "+database+".public.sensordata where ts >= NOW() - interval '"+period+" second'  and ts <= NOW()) as val WHERE sid ="+ sensorID ;
            Statement s = c.createStatement();
            ResultSet rs = s.executeQuery(query);
            while(rs.next()) {
                value = rs.getDouble("min");
                valueList.add(value);

            }
            if (valueList.size() == 0) {
                return 0.0;
            }
        } else {
            throw new Exception(ErrorsEN.DATABASE_CONNECT_FIRST);
        }
        return value;
    }

    public int getSensorID (int nodeid, int namespace, int mid) throws Exception {
        if(c.isValid(DatabaseConstants.DATABASE_TIMEOUT)) {
            String query = "SELECT sid FROM sensor WHERE nodeid = " + nodeid + " AND namespace = " + namespace + " AND mid = " + mid;
            Statement s = c.createStatement();
            ResultSet rs = s.executeQuery(query);

            rs.next();

            return rs.getInt("sid");
        } else {
            throw new Exception(ErrorsEN.DATABASE_CONNECT_FIRST);
        }
    }

    public boolean containsSensor(int nodeid, int namespace, int mid) throws Exception {
        if(c.isValid(DatabaseConstants.DATABASE_TIMEOUT)) {
            String query = "SELECT sid FROM sensor WHERE nodeid = " + nodeid + " AND namespace = " + namespace + " AND mid = " + mid;
            Statement s = c.createStatement();
            ResultSet rs = s.executeQuery(query);

            if(rs.next()) {
                return true;
            } else {
                return false;
            }
        } else {
            throw new Exception(ErrorsEN.DATABASE_CONNECT_FIRST);
        }
    }

    public boolean containsMachine(String uri) throws Exception {
        if(c.isValid(DatabaseConstants.DATABASE_TIMEOUT)) {
            String query = "SELECT mid FROM "+database+".public.machine WHERE uri = " + modifyS(uri);
            Statement s = c.createStatement();
            ResultSet rs = s.executeQuery(query);

            if(rs.next()) {
                return true;
            } else {
                return false;
            }
        } else {
            throw new Exception(ErrorsEN.DATABASE_CONNECT_FIRST);
        }
    }

    public JsonObject getSensorInfo(int sensorID) throws Exception {
        JsonObject output;
        if (c.isValid(DatabaseConstants.DATABASE_TIMEOUT)) {
            String query = "SELECT sid, mid, mname, namespace, nodeid, sname, valuetype, valuemindelta, unit, created, target FROM sensor WHERE sid = " + sensorID;
            Statement s = c.createStatement();
            ResultSet rs = s.executeQuery(query);

            rs.next();

            int id = rs.getInt("sid");
            int mid = rs.getInt("mid");
            String mname = rs.getString("mname");
            int ns = rs.getInt("namespace");
            int nid = rs.getInt("nodeid");
            String name = rs.getString("sname");
            String datatype = rs.getString("valuetype");
            String delta = rs.getString("valuemindelta");
            String unit = rs.getString("unit");
            String created = rs.getString("created");
            String target = rs.getString("target");

            query = "SELECT value, ts FROM sensorData WHERE sid = " + sensorID + " ORDER BY ts DESC LIMIT 1";
            s = c.createStatement();
            rs = s.executeQuery(query);

            String curValue;
            String ts;

            if(rs.next()) {
                curValue = rs.getString("value");
                ts = rs.getString("ts");
            } else {
                curValue = "";
                ts = "";
            }

            //TODO add calculating min max avg
            String min = "0";
            String max = "0";
            String avg = "0";

            output = JsonParser.buildJsonSensor(id, name, mid, mname, ns, nid, created, curValue, datatype, delta, unit, ts, min, max, avg);
            return output;
        } else {
            throw new Exception(ErrorsEN.DATABASE_CONNECT_FIRST);
        }
    }

    public SensorSet getAllSensors() throws Exception {
        SensorSet sensorSet = new SensorSet();
        if (c.isValid(DatabaseConstants.DATABASE_TIMEOUT)) {
            String query = "SELECT * FROM " + database + ".public.sensors";
            Statement s = c.createStatement();
            ResultSet rs = s.executeQuery(query);
            while (rs.next()) {

                int sid = rs.getInt("sid");
                int mid = rs.getInt("mid");
                String mname = rs.getString("mname");
                int namespace = rs.getInt("namespace");
                int nodeid = rs.getInt("nodeid");
                String sname = rs.getString("sname");
                String valuetype = rs.getString("valuetype");
                double valuemindelta = rs.getDouble("valuemindelta");
                String unit = rs.getString("unit");
                boolean active =  rs.getBoolean("active");
                String created =  rs.getTimestamp("created").toString();
                String target = rs.getString("target");
                boolean changed = rs.getBoolean("changed");
                sensorSet.add(sid, mid, mname, namespace, nodeid, sname, valuetype, valuemindelta, unit, active, created, target, changed);

        }

        }
        return sensorSet;
    }

    public MachineSet getAllMachines() throws Exception {
        MachineSet machineSet = new MachineSet();
        if (c.isValid(DatabaseConstants.DATABASE_TIMEOUT)) {
            String query = "SELECT * FROM " + database + ".public.machines";
            Statement s = c.createStatement();
            ResultSet rs = s.executeQuery(query);
            while (rs.next()) {
                int mid = rs.getInt("mid");
                String uri = rs.getString("uri");
                String mname = rs.getString("mname");
                boolean active = rs.getBoolean("active");
                String created = rs.getTimestamp("created").toString();
                String location = rs.getString("location");
                String description = rs.getString("description");
                boolean changed = rs.getBoolean("changed");
                machineSet.add(mid, uri, mname, active, created, location, description, changed);
            }
        }
        return machineSet;
    }

    public Sensor getSensorByID(int sid) throws Exception{
        Sensor sensor = new Sensor();
        if (c.isValid(DatabaseConstants.DATABASE_TIMEOUT)) {
            String query = "SELECT * FROM " + database + ".public.sensors WHERE sid = "+ sid;
            Statement s = c.createStatement();
            ResultSet rs = s.executeQuery(query);
            while (rs.next()) {
                int mid = rs.getInt("mid");
                String mname = rs.getString("mname");
                int namespace = rs.getInt("namespace");
                int nodeid = rs.getInt("nodeid");
                String sname = rs.getString("sname");
                String valuetype = rs.getString("valuetype");
                double valuemindelta = rs.getDouble("valuemindelta");
                String unit = rs.getString("unit");
                boolean active =  rs.getBoolean("active");
                String created =  rs.getTimestamp("created").toString();
                String target = rs.getString("target");
                boolean changed = rs.getBoolean("changed");
                sensor = new Sensor(sid, mid, mname, namespace, nodeid, sname, valuetype, valuemindelta, unit, active, created, target, changed);
            }
        }
        return sensor;
    }

    public Machine getMachineByID(int mid) throws Exception {
        Machine machine = new Machine();
        if (c.isValid(DatabaseConstants.DATABASE_TIMEOUT)) {
            String query = "SELECT * FROM " + database + ".public.machines WHERE mid = " + mid;
            Statement s = c.createStatement();
            ResultSet rs = s.executeQuery(query);
            while (rs.next()) {
                String uri = rs.getString("uri");
                String mname = rs.getString("mname");
                boolean active = rs.getBoolean("active");
                String created = rs.getTimestamp("created").toString();
                String location = rs.getString("location");
                String description = rs.getString("description");
                boolean changed = rs.getBoolean("changed");
                machine = new Machine(mid, uri, mname, active, created, location, description, changed);
            }
        }
        return machine;
    }



    public int addSensor(String sensorName, int machineID, String machineName, String valueType, String unit, int namespace, int nodeid, double delta, boolean active, boolean changed) throws Exception {
        if (c.isValid(DatabaseConstants.DATABASE_TIMEOUT)) {
            String query =
                    "INSERT INTO "+database+".public.sensor(mid, mname, namespace, nodeid, sname, valuemindelta, valuetype, unit, active, created, changed, target) VALUES ("
                            + machineID +   ", "
                            + modifyS(machineName)  +   ", "
                            + namespace +   ", "
                            + nodeid +   ", "
                            + modifyS(sensorName) + ", "
                            + delta +   ", "
                            + modifyS(valueType) +   ", "
                            + modifyS(unit) +   ", "
                            + active +   ", "
                            + "NOW()" +  ","
                            + changed + ","
                            + modifyS("database") + ");"
                    ;
            Statement s = c.createStatement();
            int done = s.executeUpdate(query);
            if(done == 1) {
                return 1;
            }
        } else {
            throw new Exception(ErrorsEN.DATABASE_CONNECT_FIRST);
        }
        return -1;
    }

    public int addMachine(String uri, String name, boolean active, String location, String description) throws Exception {
        if (c.isValid(DatabaseConstants.DATABASE_TIMEOUT)) {
            String query =
                    "INSERT INTO "+database+".public.machine(uri, mname, active, created, location, description, changed) VALUES ("
                            +modifyS(uri) + ", "
                            +modifyS(name) + ", "
                            +active + ", "
                            +"NOW(), "
                            +modifyS(location) + ", "
                            +modifyS(description) + ", "
                            +true
                            +");"
                    ;
            Statement s = c.createStatement();
            int done = s.executeUpdate(query);
            if(done == 1) {
                return 1;
            }
        } else {
            throw new Exception(ErrorsEN.DATABASE_CONNECT_FIRST);
        }
        return -1;
    }

    public int addRule(int rid, String parameter, String operation, String sensorUsed, String rname, int subrid) throws Exception{
        if (c.isValid(DatabaseConstants.DATABASE_TIMEOUT)) {
            String query = "INSERT INTO "+database+".public.rule(rid, parameter, operation, sensorUsed, rname, subrid) VALUES ("
                    +rid + ", "
                    +modifyS(parameter)+ ", "
                    +modifyS(operation)+ ", "
                    +modifyS(sensorUsed)+ ", "
                    +modifyS(rname)+ ", "
                    +subrid + ");";
            Statement s = c.createStatement();
            int done = s.executeUpdate(query);
            if(done == 1) {
                return 1;
            }
        } else {
            throw new Exception(ErrorsEN.DATABASE_CONNECT_FIRST);
        }
        return -1;
    }

    public int deleteRule(int rid) throws Exception{
        if (c.isValid(DatabaseConstants.DATABASE_TIMEOUT)) {
            String query = "DELETE FROM " + database + ".public.rule WHERE rid = " + rid;
            Statement s = c.createStatement();
            int done = s.executeUpdate(query);
            if (done == 1) {
                return 1;
            }
        } else {
                throw new Exception(ErrorsEN.DATABASE_CONNECT_FIRST);
        }
        return -1;
    }

    public int editRule(int rid, String rule, String sensorUsed) throws Exception{
        //String sensorUsed = null;
        String rname = "Rule" + rid;
        String[] arr =  rule.split(" ");
        int subrid = 1;

        /**if(c.isValid(DatabaseConstants.DATABASE_TIMEOUT)) {
            String query = "SELECT sensorUsed FROM " + database + ".public.rule WHERE rid = " + rid;
            Statement s = c.createStatement();
            ResultSet rs  = s.executeQuery(query);
            sensorUsed = rs.getString("sensorUsed");
        }**/

        int doneDelete = deleteRule(rid);
        if(doneDelete == -1){
            return -1;
        } else {
            for (int i = 0; i < arr.length; i = i + 2) {
                int doneAdd = addRule(rid, arr[i], arr[i + 1], sensorUsed, rname, subrid);
                if( doneAdd == -1 ){
                    return -1;
                } else {
                    subrid++;
                }
            }
        }
        return 1;
    }

    public int addAggregat(int aid, String aname, String parameter, String operation, String sensorUsed, int subaid) throws Exception{
        if (c.isValid(DatabaseConstants.DATABASE_TIMEOUT)) {
            String query = "INSERT INTO "+database+".public.aggregate(aid, aname, parameter, operation, sensorUsed, subaid) VALUES ("
                    +aid + ", "
                    +modifyS(aname) + ", "
                    +modifyS(parameter)+ ", "
                    +modifyS(operation)+ ", "
                    +modifyS(sensorUsed)+ ", "
                    +subaid
                    + ");";
            Statement s = c.createStatement();
            int done = s.executeUpdate(query);
            if(done == 1) {
                return 1;
            }
        } else {
            throw new Exception(ErrorsEN.DATABASE_CONNECT_FIRST);
        }
        return -1;
    }

    public int editAggregate(int aid, String aggregate, String sensorUsed) throws Exception{
        //String sensorUsed = null;
        String aname = "Aggregate" + aid;
        String[] arr =  aggregate.split(" ");
        int subaid = 1;

        /**if(c.isValid(DatabaseConstants.DATABASE_TIMEOUT)) {
         String query = "SELECT sensorUsed FROM " + database + ".public.rule WHERE rid = " + rid;
         Statement s = c.createStatement();
         ResultSet rs  = s.executeQuery(query);
         sensorUsed = rs.getString("sensorUsed");
         }**/

        int doneDelete = deleteAggregate(aid);
        if(doneDelete == -1){
            return -1;
        } else {
            for (int i = 0; i < arr.length; i = i + 2) {
                int doneAdd = addAggregat(aid, aname, arr[i], arr[i + 1], sensorUsed, subaid);
                if( doneAdd == -1 ){
                    return -1;
                } else {
                    subaid++;
                }
            }
        }
        return 1;
    }

    public int deleteAggregate(int aid) throws Exception {
        if(c.isValid(DatabaseConstants.DATABASE_TIMEOUT)) {
            String query = "DELETE FROM " + database + ".public.aggregate WHERE aid = " + aid;
            Statement s = c.createStatement();
            int done = s.executeUpdate(query);
            if(done == 1) {
                return 1;
            } else {
                throw new Exception(ErrorsEN.DATABASE_CONNECT_FIRST);
            }
        }
        return -1;
    }

    public int addEvent(int rid, String eventType, String message, boolean triggerValue)throws Exception{
        if (c.isValid(DatabaseConstants.DATABASE_TIMEOUT)) {
            String query = "INSERT into"+database+".public.event(rid, eventType, message, triggerValue) VALUES ("
                    +rid + ", "
                    +modifyS(eventType)+ ", "
                    +modifyS(message)+ ", "
                    +triggerValue+ ");";
            Statement s = c.createStatement();
            int done = s.executeUpdate(query);
            if(done == 1) {
                return 1;
            }
        } else {
            throw new Exception(ErrorsEN.DATABASE_CONNECT_FIRST);
        }
        return -1;
    }

    public int editEvent(int eid , int rid, String eventType, String message, boolean triggerValue) throws Exception {
        int doneDelete = deleteEvent(eid);
        if(doneDelete == -1){
            return -1;
        } else {
            int doneAdd = addEvent(rid, eventType, message, triggerValue);
            if(doneAdd ==  -1){
                return -1;
            }
        }
        return 1;
    }

    public int deleteEvent(int eventid) throws Exception {
        if(c.isValid(DatabaseConstants.DATABASE_TIMEOUT)) {
            String query = "DELETE FROM " + database + ".public.event WHERE eventid = " + eventid;
            Statement s = c.createStatement();
            int done = s.executeUpdate(query);
            if(done == 1) {
                return 1;
            } else {
                throw new Exception((ErrorsEN.DATABASE_CONNECT_FIRST));
            }
        }
        return -1;
    }

    public Map getActiveSensors() throws Exception {
        Map<Integer, Integer> output = new HashMap<>();
        if (c.isValid(DatabaseConstants.DATABASE_TIMEOUT)) {
            String query = "SELECT sid, mid FROM " + database + ".public.sensor WHERE active = true";

            Statement s = c.createStatement();
            ResultSet rs = s.executeQuery(query);

            while(rs.next()) {
                String curIdAsString = rs.getString("sid");
                String curMidAsString = rs.getString("mid");
                int curId = Integer.parseInt(curIdAsString);
                int curMid = Integer.parseInt(curMidAsString);
                output.put(curId, curMid);
            }
        } else {
            throw new Exception(ErrorsEN.DATABASE_CONNECT_FIRST);
        }
        return output;
    }

    public Stack getKnownMachines() throws Exception {
        Stack<Integer> output = new Stack<>();
        if(c.isValid(DatabaseConstants.DATABASE_TIMEOUT)) {
            String query = "SELECT mid FROM " + database + ".public.machine";
            Statement s = c.createStatement();
            ResultSet rs = s.executeQuery(query);

            while(rs.next()) {
                String curMidAsString = rs.getString("mid");
                int curMid = Integer.parseInt(curMidAsString);
                output.push(curMid);
            }
        } else {
            throw new Exception(ErrorsEN.DATABASE_CONNECT_FIRST);
        }
        return output;
    }

    public JsonObject getMachineInfo(int mid) throws Exception {
        JsonObject output = new JsonObject();
        if(c.isValid(DatabaseConstants.DATABASE_TIMEOUT)) {
            String query = "SELECT * FROM " + database + ".public.machine WHERE mid = " + mid;
            Statement s = c.createStatement();
            ResultSet rs = s.executeQuery(query);

            rs.next();

            int cmid = rs.getInt("mid");
            String uri = rs.getString("uri");
            String mname = rs.getString("mname");
            boolean active = rs.getBoolean("active");
            String created = rs.getString("created");
            String location = "";
            if(rs.getString("location").isEmpty()) {
                location = rs.getString("location");
            }
            String description = "";
            if(rs.getString("description").isEmpty()) {
                description = rs.getString("description");
            }

            output = JsonParser.buildJsonMachine(cmid,uri,mname,active,location,description,created);

        } else {
            throw new Exception(ErrorsEN.DATABASE_CONNECT_FIRST);
        }
        return output;
    }

    public int deactivateSensor(int sid) throws Exception {
        if(c.isValid(DatabaseConstants.DATABASE_TIMEOUT)) {
            String query = "UPDATE "+database+".public.sensor SET active = "+modifyS("false")+" WHERE sid = " + sid;
            Statement s = c.createStatement();
            int done = s.executeUpdate(query);
            if(done == 1) {
                return 1;
            }
        } else {
            throw new Exception(ErrorsEN.DATABASE_CONNECT_FIRST);
        }
        return -1;
    }

    public int deactivateMachine(int mid) throws Exception {
        if(c.isValid(DatabaseConstants.DATABASE_TIMEOUT)) {
            String query = "UPDATE "+database+".public.machine SET active = "+modifyS("false")+" WHERE mid = " + mid;
            Statement s = c.createStatement();
            int done = s.executeUpdate(query);
            if(done == 1) {
                return 1;
            }
        } else {
            throw new Exception(ErrorsEN.DATABASE_CONNECT_FIRST);
        }
        return -1;
    }

    public int getLatestRID() throws Exception {
        if(c.isValid(DatabaseConstants.DATABASE_TIMEOUT)) {
            String query = "SELECT rid FROM " + database + ".public.rule ORDER BY rid DESC";
            Statement s = c.createStatement();
            ResultSet rs = s.executeQuery(query);

            if (rs.next()) {
                return rs.getInt("rid") + 1;
            }

            return 1;
        } else {
            throw new Exception(ErrorsEN.DATABASE_CONNECT_FIRST);
        }
    }

    public int getLatestAID() throws Exception {
        if(c.isValid(DatabaseConstants.DATABASE_TIMEOUT)) {
            String query = "SELECT aid FROM " + database + ".public.aggregate ORDER BY aid DESC";
            Statement s = c.createStatement();
            ResultSet rs = s.executeQuery(query);

            if(rs.next()) {
                return rs.getInt("aid") + 1;
            }

            return 1;
        } else {
            throw new Exception(ErrorsEN.DATABASE_CONNECT_FIRST);
        }
    }

    public Map<Integer,String> getActiveSensorsByMachine(int mid) throws Exception {
        Map<Integer,String> sensors = new HashMap<>();

        if(c.isValid(DatabaseConstants.DATABASE_TIMEOUT)) {
            String query = "SELECT sid,sname FROM "+database+".public.sensor WHERE mid = "+mid+" AND active = "+modifyS("true");
            Statement s = c.createStatement();
            ResultSet rs = s.executeQuery(query);

            while(rs.next()) {
                sensors.put(rs.getInt("sid"),rs.getString("sname"));
            }
            return sensors;
        } else {
            throw new Exception(ErrorsEN.DATABASE_CONNECT_FIRST);
        }
    }

    public Stack getRID(int sid) throws Exception {
        Stack<Integer> ridStack = new Stack<>();
        String sensorID = "s"+Integer.toString(sid);
        if(c.isValid(DatabaseConstants.DATABASE_TIMEOUT)) {
            String query = "SELECT DISTINCT rid FROM " + database + ".public.rule WHERE sensorused LIKE '%;" + sensorID + ";%'";
            Statement s = c.createStatement();
            ResultSet rs = s.executeQuery(query);

            while(rs.next()) {
                int curRid = rs.getInt("rid");
                ridStack.push(curRid);
            }
        } else {
            throw new Exception(ErrorsEN.DATABASE_CONNECT_FIRST);
        }
        return ridStack;
    }

    public RuleSet getRuleSet(Stack ridStack) throws Exception {
        RuleSet ruleSet = new RuleSet();
        while(!ridStack.isEmpty()) { //changed from peek() to isEmpty()
            int ruleId = (int) ridStack.pop(); //changed from peek() to pop()
            ruleSet.addByRuleSet(getRule("R"+ruleId));

        }
        return ruleSet;
    }

    public RuleSet getRule(String rule) throws Exception {
        RuleSet ruleSet = new RuleSet();
        String ridString = rule.substring(1);
        int rid = Integer.parseInt(ridString);
        if (c.isValid(DatabaseConstants.DATABASE_TIMEOUT)) {
            String query = "SELECT parameter, operation, rid FROM " + database + ".public.rule WHERE rid = " + rid+ "ORDER BY subrid"; //changed value = rid to rid = value
            Statement s = c.createStatement();
            ResultSet rs = s.executeQuery(query);

            while (rs.next()) {
                int curRid = rs.getInt("rid");
                String curParameter = rs.getString("parameter");
                String curOperation = rs.getString("operation");
                ruleSet.add(curRid, curParameter,curOperation);



            }
        } else {
            throw new Exception(ErrorsEN.DATABASE_CONNECT_FIRST);
        }
        return ruleSet;
    }

    public AggregateSet getAggregate(String aggregate) throws Exception {
        AggregateSet aggregateSet = new AggregateSet();
        String aidString = aggregate.substring(1);
        int aid = Integer.parseInt(aidString);
        if (c.isValid(DatabaseConstants.DATABASE_TIMEOUT)) {
            String query = "SELECT parameter, operation, aid FROM " + database + ".public.aggregate WHERE aid = " + aid + "ORDER BY subaid";
            Statement s = c.createStatement();
            ResultSet rs = s.executeQuery(query);

            while (rs.next()) {
                int curAid = rs.getInt("aid");
                String curParameter = rs.getString("parameter");
                String curOperation = rs.getString("operation");

                aggregateSet.add(curAid, curParameter, curOperation);


            }
        } else {
            throw new Exception(ErrorsEN.DATABASE_CONNECT_FIRST);
        }
        return aggregateSet;
    }

    public AggregateSet getAggregateByID(int aid) throws Exception {
        AggregateSet aggregateSet = new AggregateSet();
        if (c.isValid(DatabaseConstants.DATABASE_TIMEOUT)) {
            String query = "SELECT parameter, operation, aid FROM " + database + ".public.aggregate WHERE aid = " + aid + " ORDER BY subaid";
            Statement s = c.createStatement();
            ResultSet rs = s.executeQuery(query);

            while (rs.next()) {
                int curAid = rs.getInt("aid");
                String curParameter = rs.getString("parameter");
                String curOperation = rs.getString("operation");
                aggregateSet.add(curAid, curParameter, curOperation);


            }
        } else {
            throw new Exception(ErrorsEN.DATABASE_CONNECT_FIRST);
        }
        return aggregateSet;
    }

    public EventList getEventList(RuleResultList ruleResultList) throws Exception {
        EventList eventList = new EventList();
        for(int i=0; i < ruleResultList.getSize(); i++) {
            int curRID = ruleResultList.getRID(i);
            boolean curResult = ruleResultList.getResult(i);
            if (c.isValid(DatabaseConstants.DATABASE_TIMEOUT)) {
                String query = "SELECT eventtype, message FROM " + database + ".public.event WHERE rid = " + curRID +" AND triggervalue = " + curResult;
                Statement s = c.createStatement();
                ResultSet rs = s.executeQuery(query);

                while (rs.next()) {
                    String curEventTyp = rs.getString("eventtype");
                    String curMessage = rs.getString("message");

                    eventList.add(curEventTyp, curMessage);
                }
            } else {
                throw new Exception(ErrorsEN.DATABASE_CONNECT_FIRST);
            }
        }
        return eventList;
    }

    public JsonArray getRulesList() throws Exception {
        JsonArray output = new JsonArray();

        if(c.isValid(DatabaseConstants.DATABASE_TIMEOUT)) {
            String query = "SELECT DISTINCT rid, rname FROM " + database + ".public.rule";
            Statement s = c.createStatement();
            ResultSet rs = s.executeQuery(query);

            while (rs.next()) {
                JsonObject curRule = new JsonObject();
                curRule.put("ruleID", rs.getString("rid"));
                curRule.put("ruleName", rs.getString("rname"));
                output.add(curRule);
            }
        } else {
            throw new Exception(ErrorsEN.DATABASE_CONNECT_FIRST);
        }
        return output;
    }

    public JsonArray getAggregatesList() throws Exception {
        JsonArray output = new JsonArray();

        if(c.isValid(DatabaseConstants.DATABASE_TIMEOUT)) {
            String query = "SELECT DISTINCT aid, aname FROM " + database + ".public.aggregate";
            Statement s = c.createStatement();
            ResultSet rs = s.executeQuery(query);

            while (rs.next()) {
                JsonObject curRule = new JsonObject();
                curRule.put("aggregateID", rs.getString("aid"));
                curRule.put("aggregateName", rs.getString("aname"));
                output.add(curRule);
            }
        } else {
            throw new Exception(ErrorsEN.DATABASE_CONNECT_FIRST);
        }
        return output;
    }

    private String modifyS(String s) {
        return "'"+s+"'";
    }

}

