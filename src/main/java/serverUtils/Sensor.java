package serverUtils;


public class Sensor {
    private int sid;
    private int mid;
    private String mname;
    private int nameSpace;
    private int nodeID;
    private String sname;
    private String valueType;
    private double valueMinDelta;
    private String unit;
    private boolean active;
    private String createdTimestamp;
    private String target;
    private boolean changed;

    public Sensor(int sid, int mid, String mname, int nameSpace, int nodeID, String sname, String valueType, double valueMinDelta, String unit, boolean active, String createdTimestamp, String target, boolean changed) {
        this.sid = sid;
        this.mid = mid;
        this.mname = mname;
        this.nameSpace = nameSpace;
        this.nodeID = nodeID;
        this.sname = sname;
        this.valueType = valueType;
        this.valueMinDelta = valueMinDelta;
        this.unit = unit;
        this.active = active;
        this.createdTimestamp = createdTimestamp;
        this.target = target;
        this.changed = changed;
    }

    public Sensor() {
    }


    public int getSid() {
        return sid;
    }

    public int getMid() {
        return mid;
    }

    public String getMname() {
        return mname;
    }

    public int getNameSpace() {
        return nameSpace;
    }

    public int getNodeID() {
        return nodeID;
    }

    public String getSname() {
        return sname;
    }

    public String getValueType() {
        return valueType;
    }

    public double getValueMinDelta() {
        return valueMinDelta;
    }

    public String getUnit() {
        return unit;
    }

    public boolean isActive() {
        return active;
    }

    public String getCreatedTimestamp() {
        return createdTimestamp;
    }

    public String getTarget() {
        return target;
    }

    public boolean isChanged() {
        return changed;
    }

    /**
     * ToString-Methode
     *
     * @return Gibt einen String mit RegelID, Parameter und Operation
     */
    public String toString() {
        return "Sensor{" +
                "sid=" + sid +
                ", mid=" + mid +
                ", mname='" + mname + '\'' +
                ", nameSpace=" + nameSpace +
                ", nodeID=" + nodeID +
                ", sname='" + sname + '\'' +
                ", valueType='" + valueType + '\'' +
                ", valueMinDelta=" + valueMinDelta +
                ", unit='" + unit + '\'' +
                ", active=" + active +
                ", createdTimestamp='" + createdTimestamp + '\'' +
                ", target='" + target + '\'' +
                ", changed=" + changed +
                '}';
    }

}
