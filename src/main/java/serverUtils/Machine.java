package serverUtils;

public class Machine {

    private int mid;
    private String uri;
    private String mname;
    private boolean active;
    private String createdTimestamp;
    private String location;
    private String description;
    private boolean changed;

    public Machine (int mid, String uri, String mname, boolean active, String createdTimestamp, String location, String description, boolean changed) {
        this.mid = mid;
        this.uri = uri;
        this.mname = mname;
        this. active = active;
        this.createdTimestamp = createdTimestamp;
        this.location = location;
        this.description = description;
        this.changed = changed;
    }

    public Machine () {
    }

    public int getMid() {
        return mid;
    }

    public String getUri() {
        return uri;
    }

    public String getMname() {
        return mname;
    }

    public boolean isActive() {
        return active;
    }

    public String getCreatedTimestamp() {
        return createdTimestamp;
    }

    public String getLocation() {
        return location;
    }

    public String getDescription() {
        return description;
    }

    public boolean isChanged() {
        return changed;
    }

    public String toString() {
        return "Machine{" +
                "mid=" + mid +
                ", uri='" + uri + '\'' +
                ", mname='" + mname + '\'' +
                ", active=" + active +
                ", createdTimestamp='" + createdTimestamp + '\'' +
                ", location='" + location + '\'' +
                ", description='" + description + '\'' +
                ", changed=" + changed +
                '}';
    }
}
