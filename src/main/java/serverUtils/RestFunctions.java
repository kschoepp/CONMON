package serverUtils;

import constants.DatabaseConstants;
import databaseUtils.DatabaseConnector;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.util.ArrayList;


public class RestFunctions {

    //---GET METHODS---

    public static void getAllSensors() throws Exception {
        JsonArray ja = new JsonArray();
        DatabaseConnector dc = new DatabaseConnector();
        dc.connect();
        SensorSet sensorSet = dc.getAllSensors();
        dc.close();
        for(int i = 0; i < sensorSet.getSize(); i++) {
            int sid = sensorSet.getSid(i);
            int mid = sensorSet.getMid(i);
            String mname = sensorSet.getMname(i);
            int namespace = sensorSet.getNameSpace(i);
            int nodeid = sensorSet.getNodeID(i);
            String sname = sensorSet.getSname(i);
            String valuetype = sensorSet.getValueType(i);
            double valuemindelta = sensorSet.getValueMinDelta(i);
            String unit = sensorSet.getUnit(i);
            boolean active =  sensorSet.isActive(i);
            String created =  sensorSet.getCreatedTimestamp(i);
            String target = sensorSet.getTarget(i);
            boolean changed = sensorSet.isChanged(i);
            ja.add(sid);
            ja.add(mid);
            ja.add(mname);
            ja.add(namespace);
            ja.add(nodeid);
            ja.add(sname);
            ja.add(valuetype);
            ja.add(valuemindelta);
            ja.add(unit);
            ja.add(active);
            ja.add(created);
            ja.add(target);
            ja.add(changed);
        }

        //TODO write JSON array into webserver body
    }

    public static void getSensorById(int id) throws Exception{
//        JsonArray ja = new JsonArray();
        JsonObject jo = new JsonObject();
        DatabaseConnector dc = new DatabaseConnector();
        dc.connect();
        Sensor sensor = dc.getSensorByID(id);
        dc.close();
        jo.put("sid", sensor.getSid());
        jo.put("mid", sensor.getMid());
        jo.put("mname", sensor.getMname());
        jo.put("namespace", sensor.getNameSpace());
        jo.put("nodeid", sensor.getNodeID());
        jo.put("sname", sensor.getSname());
        jo.put("valuetyppe", sensor.getValueType());
        jo.put("valuemindelta", sensor.getValueMinDelta());
        jo.put("unit", sensor.getUnit());
        jo.put("active", sensor.isActive());
        jo.put("created", sensor.getCreatedTimestamp());
        jo.put("target", sensor.getTarget());
        jo.put("changed", sensor.isChanged());
//        ja.add(sensor.getSid());
//        ja.add(sensor.getMid());
//        ja.add(sensor.getMname());
//        ja.add(sensor.getNameSpace());
//        ja.add(sensor.getNodeID());
//        ja.add(sensor.getSname());
//        ja.add(sensor.getValueType());
//        ja.add(sensor.getValueMinDelta());
//        ja.add(sensor.getUnit());
//        ja.add(sensor.isActive());
//        ja.add(sensor.getCreatedTimestamp());
//        ja.add(sensor.getTarget());
//        ja.add(sensor.isChanged());

        //TODO write JSON object into webserver body

    }

    public static void getMachines() throws Exception{
        JsonArray ja = new JsonArray();
        DatabaseConnector dc = new DatabaseConnector();
        dc.connect();
        MachineSet machineSet = dc.getAllMachines();
        dc.close();
        for (int i = 0; i < machineSet.getSize(); i++) {
            int mid = machineSet.getMid(i);
            String uri = machineSet.getUri(i);
            String mname = machineSet.getMname(i);
            boolean active = machineSet.isActive(i);
            String created = machineSet.getCreatedTimestamp(i);
            String location = machineSet.getLocation(i);
            String description = machineSet.getDescription(i);
            boolean changed = machineSet.isChanged(i);
            ja.add(mid);
            ja.add(uri);
            ja.add(mname);
            ja.add(active);
            ja.add(created);
            ja.add(location);
            ja.add(description);
            ja.add(changed);
        }

        //TODO write JSON array into webserver body
    }

    public static void getMachineById(int id) throws Exception {
        //TODO add code to get machine data by a given id and write them as JSON object into the webserver body
        JsonObject jo = new JsonObject();
        DatabaseConnector dc = new DatabaseConnector();
        dc.connect();
        Machine machine = dc.getMachineByID(id);
        dc.close();
        jo.put("mid", machine.getMid());
        jo.put("uri", machine.getUri());
        jo.put("mname", machine.getMname());
        jo.put("active", machine.isActive());
        jo.put("created", machine.getCreatedTimestamp());
        jo.put("location", machine.getLocation());
        jo.put("description", machine.getDescription());
        jo.put("changed", machine.isChanged());

        //TODO write JSON object into webserver body
    }

    //---POST METHODS---

    public static boolean addSensor() {
        //TODO add functionality to add a sensor to the database
        return false;
    }

}
