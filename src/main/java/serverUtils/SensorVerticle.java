package serverUtils;

import mailUtils.MailUtils;
import configUtils.ServerCfgReader;
import configUtils.SmtpCfgReader;
import databaseUtils.DatabaseConnector;
import io.vertx.core.AbstractVerticle;
import ruleSystem.EventList;
import ruleSystem.RuleCheck;


public class SensorVerticle extends AbstractVerticle {

    //Local variables
    private int id;
    //CONMON classes
    DatabaseConnector db;
    //Dynamic local variables
    private String curValue;
    private String lastValue;

    public SensorVerticle(int id) throws Exception {
        this.db = new DatabaseConnector();
        this.db.connect();
        this.id = id;
        this.curValue = db.getSensorValue(this.id);
        this.db.close();
    }

    /**
     * Start-Method for VERT.X Verticles
     */
    public void start() throws Exception {
        vertx.setPeriodic(ServerCfgReader.getPeriodicTime(), id -> {
                try {
                    this.db.connect();
                    checkUpdate();
                    this.db.close();
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
            }
        );
    }

    private void checkUpdate() throws Exception {
        String newValue = db.getSensorValue(this.id);
        if(!curValue.equals(newValue)) {
            lastValue = curValue;
            curValue = newValue;
            System.out.println("New value for sensor with ID: " + id + ", new Value: " + curValue);
            EventList el = RuleCheck.startRuleCheck(this.id);
            if(el == null){
                throw new NullPointerException();
            }
            for(int i = 0; i < el.getSize(); i++) {
                if(el.getEventListObject(i).getEventTyp().equals("mail")) {
                    String message = el.getEventListObject(i).getMessage();
                    MailUtils.sendMail(SmtpCfgReader.getConfig("smtpHost"), SmtpCfgReader.getConfig("username"),SmtpCfgReader.getConfig("password"),SmtpCfgReader.getConfig("senderAddress"),SmtpCfgReader.getConfig("targetAddress"),"Warning",message);
                    System.out.println("Done");
                }
            }
        }
    }

}
