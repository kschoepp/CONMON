package serverUtils;

import java.util.ArrayList;
import java.util.List;

public class MachineSet {
    List<Machine> machines = new ArrayList<>();

    public MachineSet (int mid, String uri, String mname, boolean active, String createdTimestamp, String location, String description, boolean changed) {
        machines.add(new Machine(mid, uri, mname, active, createdTimestamp, location, description, changed));
    }

    public MachineSet () {
    }

    public void add (int mid, String uri, String mname, boolean active, String createdTimestamp, String location, String description, boolean changed) {
        machines.add(new Machine(mid, uri, mname, active, createdTimestamp, location, description, changed));
    }

    public int getMid (int index) {
        int mid = machines.get(index).getMid();
        return mid;
    }

    public String getUri (int index) {
        String uri = machines.get(index).getUri();
        return uri;
    }

    public String getMname (int index) {
        String mname = machines.get(index).getMname();
        return mname;
    }

    public boolean isActive (int index) {
        boolean active = machines.get(index).isActive();
        return active;
    }

    public String getCreatedTimestamp (int index) {
        String createdTimestamp = machines.get(index).getCreatedTimestamp();
        return createdTimestamp;
    }

    public String getLocation (int index) {
        String location = machines.get(index).getLocation();
        return location;
    }

    public String getDescription (int index) {
        String description = machines.get(index).getDescription();
        return description;
    }

    public boolean isChanged (int index) {
        boolean changed = machines.get(index).isChanged();
        return changed;
    }

    public int getSize() {
        return machines.size();
    }
}
