package serverUtils;

import configUtils.ServerCfgReader;
import databaseUtils.DatabaseConnector;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import jsonUtils.JsonParser;
import locales.ErrorsEN;
import locales.MessagesEN;

import java.io.IOException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.*;



public class RestServer extends AbstractVerticle {

    //Config variables
    private int port;
    //Local variables
    private List<Integer> knownMachines = new ArrayList<>();
    private Map<Integer, String> activeSensors = new HashMap<>();
    //CONMON Classes
    private DatabaseConnector db;
    //VERT.X Objects
    private HttpServer httpServer;
    private Router router;
    //TMP variables
    private String curDeployID;

    //Constructors
    public RestServer() throws Exception {
        this.port = ServerCfgReader.getServerPort();
        db = new DatabaseConnector();
        String result = db.connect();
        System.out.println(result);
    }

    public static void main(String[] args) {
        try {
            VertxOptions options = new VertxOptions();
            options.setBlockedThreadCheckInterval(1000 * 60 * 60);
            Vertx vertx = Vertx.vertx(options);
            vertx.deployVerticle(new RestServer());
        } catch (IOException io) {
            //Ignore
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    private void reloadOnStart() throws Exception {
        if (db.isConnected()) {
            Stack<Integer> machines = db.getKnownMachines();
            Map<Integer, Integer> sensors = db.getActiveSensors();
            while (!machines.empty()) {
                int curMid = machines.pop();
                knownMachines.add(curMid);
            }
            Iterator<Integer> sids = sensors.keySet().iterator();
            while (sids.hasNext()) {
                int curSid = sids.next();
                int curMid = sensors.get(curSid);
                if (knownMachines.contains(curMid)) {
                    this.curDeployID = "errDeploy";
                    vertx.deployVerticle(new SensorVerticle(curSid), res -> {
                                if (res.succeeded()) {
                                    this.curDeployID = res.result();
                                    activeSensors.put(curSid, curDeployID);
                                    //Console-Output
                                    System.out.println(MessagesEN.SENSOR_DEPLOYED_1 + curDeployID + MessagesEN.SENSOR_DEPLOYED_2);
                                } else {
                                    //Console-Output
                                    System.out.println(ErrorsEN.SENSOR_DEPLOY_FAILED_1 + curSid + ErrorsEN.SENSOR_DEPLOY_FAILED_2);
                                }
                            }
                    );
                } else {
                    System.out.println(ErrorsEN.RESTART_S_HAS_NO_KNOWN_M);
                }

            }

        } else {
            System.out.println(ErrorsEN.RESTART_M_AND_S_NOT_LOADABLE);
        }
    }

    public void start(Future<Void> future) throws Exception {
        router = Router.router(vertx);

        //Reload active sensors from database and deploy them
        reloadOnStart();

        //Server response for the webroot directory
        router.route("/").handler(routingContext -> {
                    HttpServerResponse response = routingContext.response();
                    response.putHeader("content-type", "text/html").sendFile(
                            "./assets/index.html"
                    );
                }
        );

        //Building REST-API functions
        //---GET---
        router.get("/api/sensors").handler(this::getAllSensors);
        router.get("/api/sensors/:id").handler(this::getSensorById);
        router.get("/api/sensors/bymachine/:id").handler(this::getSensorsByMachine);
        router.get("/api/machines").handler(this::getAllMachines);
        router.get("/api/machines/:id").handler(this::getMachineById);
        router.get("/api/rules").handler(this::getAllRules);
        router.get("/api/rules/id").handler(this::getLatestRuleID);
        router.get("/api/aggregates").handler(this::getAllAggregates);
        router.get("/api/aggregates/id").handler(this::getLatestAID);
        //---POST---
        router.post("/api/sensors").handler(this::addSensor);
        router.post("/api/machines").handler(this::addMachine);
        router.post("/api/rules").handler(this::addRule);
        router.post("/api/aggregate").handler(this::addAggregate);
        router.post("/api/event").handler(this::addEvent);
        //---DELETE---
        router.delete("/api/sensors/:id").handler(this::deleteSensor);
        router.delete("/api/machines/:id").handler(this::deleteMachine);
        router.delete("/api/rule/:id").handler(this::deleteRule);
        router.delete("/api/aggregate/:id").handler(this::deleteAggregate);
        router.delete("/api/event/:id").handler(this::deleteEvent);
        //---PUT---
        router.put("/api/rule").handler(this::editRule);
        router.put("/api/aggregate").handler(this::editAggregate);
        router.put("/api/event").handler(this::editEvent);

        //Starting HTTP-Server
        httpServer = vertx.createHttpServer();
        httpServer.requestHandler(router::accept).listen(port,
                result -> {
                    if (result.succeeded()) {
                        future.complete();
                    } else {
                        future.fail(result.cause());
                    }
                }
        );
    }

    //---GET METHODS---START---

    private void getAllRules(RoutingContext rc) {
        try {
            JsonArray output = db.getRulesList();
            rc.response()
                    .setStatusCode(201)
                    .putHeader("content-type", "application/json; charset=utf-8")
                    .end(Json.encodePrettily(output));
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    private void getAllAggregates(RoutingContext rc) {
        try {
            JsonArray output = db.getAggregatesList();
            rc.response()
                    .setStatusCode(201)
                    .putHeader("content-type", "application/json; charset=utf-8")
                    .end(Json.encodePrettily(output));
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    private void getAllSensors(RoutingContext rc) {
        try {
            JsonArray output = new JsonArray();

            if (activeSensors.size() > 0) {
                Object[] keys = activeSensors.keySet().toArray();
                for (int i = 0; i < keys.length; i++) {
                    int curKey = Integer.parseInt(keys[i].toString());
                    JsonObject curJSON = db.getSensorInfo(curKey);
                    output.add(curJSON);
                }
                rc.response()
                        .setStatusCode(201)
                        .putHeader("content-type", "application/json; charset=utf-8")
                        .end(Json.encodePrettily(output));
            } else {
                rc.response()
                        .setStatusCode(404)
                        .putHeader("content-type", "application/json; charset=utf-8")
                        .end(Json.encodePrettily(JsonParser.buildJsonResponse("error", ErrorsEN.FRONTEND_SENSORS_NOT_FOUND)));
            }
        } catch (Exception e) {
            //System.out.println(e.getMessage());
        }
    }

    private void getSensorById(RoutingContext rc) {
        try {
            int sid = Integer.parseInt(rc.request().getParam("id"));
            JsonObject sensor = db.getSensorInfo(sid);
            rc.response()
                    .setStatusCode(201)
                    .putHeader("content-type", "application/json; charset=utf-8")
                    .end(Json.encodePrettily(sensor));
        } catch (Exception e) {
            rc.response()
                    .setStatusCode(400)
                    .putHeader("content-type", "text/html")
                    .end(e.getMessage());
        }
    }

    private void getSensorsByMachine(RoutingContext rc) {
        try {
            int mid = Integer.parseInt(rc.request().getParam("id"));
            Map<Integer, String> activeSensors = db.getActiveSensorsByMachine(mid);
            JsonArray output = new JsonArray();
            Set<Integer> keySet = activeSensors.keySet();
            Iterator<Integer> keyIt = keySet.iterator();
            while (keyIt.hasNext()) {
                int sid = keyIt.next();
                JsonObject curJson = JsonParser.buildJsonSensorListItem(sid, activeSensors.get(sid));
                output.add(curJson);
            }
            rc.response()
                    .setStatusCode(201)
                    .putHeader("content-type", "application/json; charset=utf-8")
                    .end(Json.encodePrettily(output));
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    private void getAllMachines(RoutingContext rc) {
        try {
            JsonArray output = new JsonArray();
            if (knownMachines.size() > 0) {
                ListIterator<Integer> keys = knownMachines.listIterator();
                while (keys.hasNext()) {
                    int curKey = keys.next();
                    JsonObject curMachine = db.getMachineInfo(curKey);
                    output.add(curMachine);
                }
                rc.response()
                        .setStatusCode(201)
                        .putHeader("content-type", "application/json; charset=utf-8")
                        .end(Json.encodePrettily(output));
            } else {
                rc.response()
                        .setStatusCode(400)
                        .putHeader("content-type", "application/json; charset=utf-8")
                        .end(Json.encodePrettily(JsonParser.buildJsonResponse("error", ErrorsEN.FRONTEND_MACHINES_NOT_FOUND)));
            }
        } catch (Exception e) {
            //System.out.println(e.getMessage());
        }
    }

    private void getMachineById(RoutingContext rc) {
        try {
            int mid = Integer.parseInt(rc.request().getParam("id"));
            JsonObject machine = db.getMachineInfo(mid);
            rc.response()
                    .setStatusCode(201)
                    .putHeader("content-type", "application/json; charset=utf-8")
                    .end(Json.encodePrettily(machine));
        } catch (Exception e) {
            rc.response()
                    .setStatusCode(400)
                    .putHeader("content-type", "text/html")
                    .end(e.getMessage());
        }
    }

    //---GET METHODS---END---
    //---POST METHODS---START---

    private void addSensor(RoutingContext rc) {
        int mid = Integer.parseInt(rc.request().getParam("machineID"));
        if (knownMachines.contains(mid)) {
            this.curDeployID = "errDeploy";
            try {
                String sensorName = rc.request().getParam("sensorName");
                String machineName = rc.request().getParam("machineName");
                int namespace = Integer.parseInt(rc.request().getParam("namespace"));
                int nodeid = Integer.parseInt(rc.request().getParam("nodeid"));
                String valueType = rc.request().getParam("valueType");
                double delta = Double.parseDouble(rc.request().getParam("delta"));
                String unit = rc.request().getParam("unit");
                if(db.containsSensor(nodeid,namespace,mid)) {
                    System.out.println(ErrorsEN.SENSOR_ADD_FAILED_ALREADY_EXISTS_1+nodeid+ErrorsEN.SENSOR_ADD_FAILED_ALREADY_EXISTS_2+namespace+ErrorsEN.SENSOR_ADD_FAILED_ALREADY_EXISTS_3+mid+ErrorsEN.SENSOR_ADD_FAILED_ALREADY_EXISTS_4);
                    rc.response().setStatusCode(400).putHeader("content-type", "text/html; charset=utf-8")
                            .end("-3");
                } else {
                    db.addSensor(sensorName, mid, machineName, valueType, unit, namespace, nodeid, delta, true, true);
                    int sid = db.getSensorID(nodeid, namespace, mid);

                    if (activeSensors.containsKey(sid)) {
                        System.out.println(ErrorsEN.SENSOR_ALREADY_ACTIVE_1 + sid + ErrorsEN.SENSOR_ALREADY_ACTIVE_2);
                        rc.response().setStatusCode(400).putHeader("content-type", "text/html; charset=utf-8")
                                .end("-2");
                    } else {
                        vertx.deployVerticle(new SensorVerticle(sid), res -> {
                            if (res.succeeded()) {
                                this.curDeployID = res.result();
                                System.out.println(MessagesEN.SENSOR_DEPLOYED_1 + curDeployID + MessagesEN.SENSOR_DEPLOYED_2);
                                activeSensors.put(sid, curDeployID);
                                rc.response().setStatusCode(200).putHeader("content-type", "text/html; charset=utf-8")
                                        .end("1");
                            } else {
                                //Console-Output
                                System.out.println(ErrorsEN.SENSOR_DEPLOY_FAILED_1 + sid + ErrorsEN.SENSOR_DEPLOY_FAILED_2);
                                //REST-Output
                                rc.response().setStatusCode(400).putHeader("content-type", "text/html; charset=utf-8")
                                        .end("-1");
                            }
                        });
                    }
                }
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        } else {
            System.out.println("No active machine found to the given machineID <"+ mid +">!");
        }
    }

    private void addMachine(RoutingContext rc) {
        try {
            String uri = rc.request().getParam("uri");
            uri = URLDecoder.decode(uri,"UTF-8");
            if (db.containsMachine(uri)) {
                System.out.println(); //TODO machine with this uri already exists
                rc.response().setStatusCode(400).putHeader("content-type", "text/html; charset=utf-8")
                        .end("-3");
            } else {
                String machineName = URLDecoder.decode(rc.request().getParam("machineName"),"UTF-8");
                String description = URLDecoder.decode(rc.request().getParam("description"),"UTF-8");
                String location = URLDecoder.decode(rc.request().getParam("location"),"UTF-8");
                int success = db.addMachine(uri,machineName,true,location,description);
                if(success == 1) {
                    System.out.println("Machine <" + machineName + "> successfully added!");
                    rc.response().setStatusCode(200).end();
                } else {
                    rc.response().setStatusCode(400).end();
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    private void getLatestRuleID(RoutingContext rc) {
        try {
            int rid = db.getLatestRID();
            rc.response().setStatusCode(200).end(Integer.toString(rid));
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    private void getLatestAID(RoutingContext rc) {
        try {
            int rid = db.getLatestAID();
            rc.response().setStatusCode(200).end(Integer.toString(rid));
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    private void addRule(RoutingContext rc) {
        try {
            int rid = Integer.parseInt(rc.request().getParam("rid"));
            String param = rc.request().getParam("param");
            String operator = rc.request().getParam("operator");
            String sensorused = URLDecoder.decode(rc.request().getParam("sensorused"),"UTF-8");
            String ruleName = rc.request().getParam("ruleName");
            int subrid = Integer.parseInt(rc.request().getParam("subrid"));
            if(ruleName.isEmpty()) {
                ruleName = "Rule"+rid;
            }
            if(param.equals("END")) {
                param = "#";
            }
            int success = db.addRule(rid,param,operator,sensorused, ruleName, subrid);
            if(success == 1) {
                System.out.println("New rule with ID <" + rid + "> added!");
                rc.response().setStatusCode(200).end();
            } else {
                rc.response().setStatusCode(400).end();
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    private void addAggregate(RoutingContext rc) {
        try {
            int aid = Integer.parseInt(rc.request().getParam("aid"));
            String aname = rc.request().getParam("aname");
            String parameter = rc.request().getParam("parameter");
            String operation = rc.request().getParam("operator");
            String sensorUsed = URLDecoder.decode(rc.request().getParam("sensorused"), "UTF-8");
            int subaid = Integer.parseInt(rc.request().getParam("subaid"));
            if(aname.isEmpty()) {
                aname = "Aggregate"+aid;
            }
            if(parameter.equals("END")) {
                parameter = "#";
            }
            int success = db.addAggregat(aid, aname, parameter, operation, sensorUsed, subaid);
            if(success == 1) {
                System.out.println("New aggregate with ID <" + aid + "> added!");
                rc.response().setStatusCode(200).end();
            } else {
                rc.response().setStatusCode(400).end();
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    private void addEvent(RoutingContext rc) {
        try {
            int rid = Integer.parseInt(rc.request().getParam("rid"));
            String eventType = rc.request().getParam("eventType");
            String message = rc.request().getParam("message");
            boolean triggerValue = Boolean.getBoolean(rc.request().getParam("triggerValue"));
            int success = db.addEvent(rid, eventType, message, triggerValue);
            if(success == 1) {
                rc.response().setStatusCode(200).end();
            }
            else {
                rc.response().setStatusCode(400).end();
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    //---POST METHODS---END---


    //---DELETE METHODS---START---

    private void deleteSensor(RoutingContext rc) {
        int id = Integer.parseInt(rc.request().getParam("id"));
        if (!activeSensors.containsKey(id)) {
            rc.response()
                    .setStatusCode(404)
                    .putHeader("content-type", "text/html; charset=utf-8")
                    .end("failed");
        } else {
            String dID = activeSensors.get(id);
            vertx.undeploy(dID, res -> {
                        if (res.succeeded()) {
                            activeSensors.remove(id);
                            try {
                                db.deactivateSensor(id);
                            } catch (Exception e) {
                                System.out.println(e.getMessage());
                            }
                            //Console-Output
                            System.out.println(MessagesEN.SENSOR_UNDEPLOYED_1 + dID + MessagesEN.SENSOR_UNDEPLOYED_2);
                            //REST-Output
                            rc.response()
                                    .setStatusCode(201)
                                    .putHeader("content-type", "text/html; charset=utf-8")
                                    .end("success");
                        } else {
                            //Console-Output
                            System.out.println(ErrorsEN.SENSOR_UNDEPLOY_FAILED_1 + dID + ErrorsEN.SENSOR_UNDEPLOY_FAILED_2);
                            //REST-Output
                            rc.response()
                                    .setStatusCode(404)
                                    .putHeader("content-type", "text/html; charset=utf-8")
                                    .end("failed");
                        }
                    }
            );
        }
    }

    private void deleteMachine(RoutingContext rc) {
        int id = Integer.parseInt(rc.request().getParam("id"));
        try {
            Map<Integer, String> sensors = db.getActiveSensorsByMachine(id);
            if (sensors.size() > 0) {
                rc.response()
                        .setStatusCode(405)
                        .putHeader("content-type", "text/html; charset=utf-8")
                        .end("errorSensorsActive");
            } else {
                int pos = knownMachines.indexOf(id);
                knownMachines.remove(pos);
                int success = db.deactivateMachine(id);
                if (success == 1) {
                    System.out.println(MessagesEN.MACHINE_DEACTIVATED_1 + id + MessagesEN.MACHINE_DEACTIVATED_2);
                    rc.response()
                            .setStatusCode(201)
                            .putHeader("content-type", "text/html; charset=utf-8")
                            .end("success");
                } else {
                    rc.response()
                            .setStatusCode(400)
                            .putHeader("content-type", "text/html; charset=utf-8")
                            .end("failed");
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    private void deleteRule(RoutingContext rc) {
        int id = Integer.parseInt(rc.request().getParam("id"));
        try{
            int success = db.deleteRule(id);

            if(success == 1){
                System.out.println("Rule with <" + id + "> was deleted");
                rc.response().setStatusCode(200);
            } else {
                rc.response().setStatusCode(404);
            }
        }
        catch(Exception e){
            System.out.println(e.getMessage());
        }
    }

    private void deleteAggregate(RoutingContext rc) {
        int aid = Integer.parseInt(rc.request().getParam("id"));
        try {
            int success = db.deleteAggregate(aid);

            if(success == 1) {
                System.out.println("Aggregate with ID <" + aid + "> was deleted.");
                rc.response().setStatusCode(200);
            } else {
                rc.response().setStatusCode(404);
            }
        } catch(Exception e) {
            System.out.println(e.getMessage());
        }
    }

    private void deleteEvent(RoutingContext rc) {
        int eventid = Integer.parseInt(rc.request().getParam("eventid"));
        try {
            int success = db.deleteEvent(eventid);

            if(success == 1) {
                System.out.println("Even with ID <" + eventid + "> was deleted.");
                rc.response().setStatusCode(200);
            } else {
                rc.response().setStatusCode(404);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    //---DELETE METHODS---END---

    //---PUT METHODS---START---

    private void editRule(RoutingContext rc) {
        int id = Integer.parseInt(rc.request().getParam("id"));
        String rule = rc.request().getParam("rule");
        String sensorUsed = rc.request().getParam("sensorUsed");

        try{
            int success = db.editRule(id,rule,sensorUsed);
            if(success == 1){
                System.out.println("Rule with >" + id + "was edit");
                rc.response().setStatusCode(200);
            } else {
                rc.response().setStatusCode(404);
            }

        } catch (Exception e){
            System.out.println(e.getMessage());
        }
    }

    private void editAggregate(RoutingContext rc) {
        int id = Integer.parseInt(rc.request().getParam("id"));
        String aggregate = rc.request().getParam("aggregate");
        String sensorUsed = rc.request().getParam("sensorUsed");

        try{
            int success = db.editAggregate(id,aggregate,sensorUsed);
            if(success == 1){
                System.out.println("Aggregate with >" + id + "was edit");
                rc.response().setStatusCode(200);
            } else {
                rc.response().setStatusCode(404);
            }

        } catch (Exception e){
            System.out.println(e.getMessage());
        }
    }

    private void editEvent(RoutingContext rc) {
        int eid = Integer.parseInt(rc.request().getParam("eid"));
        int rid = Integer.parseInt(rc.request().getParam("rid"));
        String eventType = rc.request().getParam("eventType");
        String message = rc.request().getParam("message");
        boolean triggerValue = Boolean.parseBoolean(rc.request().getParam("triggerValue"));

        try{
            int success = db.editEvent(eid, rid, eventType, message, triggerValue);
            if (success == 1){
                System.out.println("Event with >" + eid + "was edit");
                rc.response().setStatusCode(200);
            } else {
                rc.response().setStatusCode(404);
            }

        } catch (Exception e){
            System.out.println(e.getMessage());
        }
    }

    //---PUT METHODS---END---

}
