package serverUtils;

import java.util.ArrayList;
import java.util.List;


public class SensorSet {
    List<Sensor> sensors = new ArrayList<Sensor>();


    public SensorSet(int sid, int mid, String mname, int nameSpace, int nodeID, String sname, String valueType, double valueMinDelta, String unit, boolean active, String createdTimestamp, String target, boolean changed){

        sensors.add(new Sensor(sid, mid, mname, nameSpace, nodeID, sname, valueType, valueMinDelta, unit, active, createdTimestamp, target, changed));
    }


    public SensorSet(){

    }


    public void add(int sid, int mid, String mname, int nameSpace, int nodeID, String sname, String valueType, double valueMinDelta, String unit, boolean active, String createdTimestamp, String target, boolean changed) {


        sensors.add(new Sensor(sid, mid, mname, nameSpace, nodeID, sname, valueType, valueMinDelta, unit, active, createdTimestamp, target, changed));
    }

    public int getSid(int index) {
        int sid = sensors.get(index).getSid();
        return sid;
    }

    public int getMid(int index) {
        int mid = sensors.get(index).getMid();
        return mid;
    }

    public String getMname(int index) {
        String mname = sensors.get(index).getMname();
        return mname;
    }

    public int getNameSpace(int index) {
        int nameSpace = sensors.get(index).getNameSpace();
        return nameSpace;
    }

    public int getNodeID(int index) {
        int nodeID = sensors.get(index).getNodeID();
        return nodeID;
    }

    public String getSname(int index) {
        String sname = sensors.get(index).getSname();
        return sname;
    }

    public String getValueType(int index) {
        String valueType = sensors.get(index).getValueType();
        return valueType;
    }

    public double getValueMinDelta(int index) {
        double valueMinDelta = sensors.get(index).getValueMinDelta();
        return valueMinDelta;
    }

    public String getUnit(int index) {
        String unit = sensors.get(index).getUnit();
        return unit;
    }

    public boolean isActive(int index) {
        boolean active = sensors.get(index).isActive();
        return active;
    }

    public String getCreatedTimestamp(int index) {
        String createdTimestamp = sensors.get(index).getCreatedTimestamp();
        return createdTimestamp;
    }

    public String getTarget(int index) {
        String target = sensors.get(index).getTarget();
        return target;
    }

    public boolean isChanged(int index) {
        boolean changed = sensors.get(index).isChanged();
        return changed;
    }


    public int getSize() {
        return sensors.size();
    }
}
