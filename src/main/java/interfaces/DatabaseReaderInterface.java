package interfaces;

import io.vertx.core.json.JsonObject;


public interface DatabaseReaderInterface {

    /**
     * Function to connect to a database
     * @return Return 1 if successfully connected, -1 if connection failed
     */
    String connect() throws Exception;

    /**
     * Function to check if you are already connected to the database
     * @return true = connected | false = not connected
     */
    boolean isConnected() throws Exception;

    /**
     * Function to get the current value of an sensor
     * @param sensorID ID of the desired sensor
     * @return Value in String format
     */
    String getSensorValue(int sensorID) throws Exception;

    /**
     * Returns all information about a sensor with a JSON-Object
     * @param sensorID ID of the desired sensor
     * @return JSON-Object filled with sensor data
     */
    JsonObject getSensorInfo(int sensorID) throws Exception;

    /**
     * Function to add a new sensor to the database
     * @param sensorName Name of the sensor as a String
     * @param machineID ID of the connected machine as an Integer
     * @param machineName Name of the connected machine as a String
     * @param valueType
     * @param unit
     * @return
     */
    int addSensor(String sensorName, int machineID, String machineName, String valueType, String unit, int namespace, int nodeid, double delta, boolean active, boolean changed) throws Exception;

    /**
     * Function to add a new machine to the database
     * @param uri URI to the machine server
     * @param name Name of the machine
     * @param active Status if the machine is active and connectable
     * @param location Location as a String
     * @param description Description as a String
     * @return 1 - machine added | -1 adding the machine failed
     */
    int addMachine(String uri, String name, boolean active, String location, String description) throws Exception;



}