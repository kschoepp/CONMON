package jsonUtils;

import io.vertx.core.json.JsonObject;


public class JsonParser {

    public static JsonObject buildJsonSensor(int sensorID, String sensorName, int machineID, String machineName, int namespace, int nodeid, String createTS, String value, String valueType, String delta, String unit, String timeStamp, String min, String max, String avg) {

        JsonObject jsonObject = new JsonObject();
        jsonObject.put("sensorID", sensorID);
        jsonObject.put("sensorName", sensorName);
        jsonObject.put("machineID", machineID);
        jsonObject.put("machineName", machineName);
        jsonObject.put("namespace", namespace);
        jsonObject.put("nodeid", nodeid);
        jsonObject.put("createTS", createTS);
        jsonObject.put("value", value);
        jsonObject.put("valueType", valueType);
        jsonObject.put("deltaValue", delta);
        jsonObject.put("unit", unit);
        jsonObject.put("timeStamp", timeStamp);
        jsonObject.put("min", min);
        jsonObject.put("max", max);
        jsonObject.put("avg", avg);
        return jsonObject;


    }

    public static JsonObject buildJsonMachine(int machineID, String URI, String machineName, boolean isActive, String location, String description, String createTS) {

        JsonObject jsonObject = new JsonObject();
        jsonObject.put("machineID", machineID);
        jsonObject.put("URI", URI);
        jsonObject.put("machineName", machineName);
        jsonObject.put("isActive", isActive);
        jsonObject.put("location", location);
        jsonObject.put("description", description);
        jsonObject.put("createTS", createTS);
        return jsonObject;

    }

    public static JsonObject buildJsonResponse(String responseType, String message) {
        JsonObject output = new JsonObject();
        output.put("responseType", responseType);
        output.put("message", message);
        return output;
    }

    public static JsonObject buildJsonSensorListItem(int id, String sname) {
        JsonObject output = new JsonObject();
        output.put("sensorID",id);
        output.put("sensorName",sname);
        return output;
    }

}