package ruleSystem;

/**
 *
 * Created by Kathrin Schoepp on 08.09.2017
 **/
public class Rule {
    private int rid;
    private String parameter;
    private String operation;
    private int subRid;

    /**
     * Konstruktor fuer eine Regel
     * @param rid  Die ID einer Regel
     * @param parameter Ein Parameter einer Regel
     * @param operation Ein Operator einer Regel
     */
    public Rule(int rid, String parameter, String operation){
        this.rid = rid;
        this.parameter = parameter;
        this.operation = operation;
    }

    /**
     * Kronstruktor fuer eine Regel mit zusätzlicher SubruleID
     * @param rid Die ID einer Regel
     * @param parameter Ein Parameter einer Regel
     * @param operation Eine Operation einer Regel
     * @param subRid Eine UnterID einer Regel
     */
    public Rule(int rid, String parameter, String operation, int subRid) {
        this.rid = rid;
        this.parameter = parameter;
        this.operation = operation;
        this.subRid = subRid;
    }

    /**
     * Gibt einen Parameter zurueck
     * @return  Einen Parameter der Regel
     */
    public String getParameter(){
        return parameter;
    }

    /**
     * Gibt eine UnterID zurueck
     * @return Eine Zahl als UnterID
     */
    public int getSubRid() {
        return subRid;
    }

    /**
     * Gibt einen Operator zurueck
     * @return  Einen Operator der Regel
     */
    public String getOperation(){

        return operation;
    }

    /**
     * Gibt die ID einer Regel zurueck
     * @return  Eine Zahl (Eine ID einer Regel)
     */
    public int getRid(){
        return rid;
    }

    /**
     * ToString-Methode
     * @return Gibt einen String mit RegelID, Parameter und Operation
     */
    public String toString(){
        String s = "Rid:"+ rid + "Parameter:"+ parameter + "Operation:" + operation;
        return s;
    }
}
