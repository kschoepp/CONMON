package ruleSystem;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * Created by Kathrin Schoepp on 08.09.2017
 **/
public class RuleResultList {

    List<Result> ruleResults = new ArrayList<Result>();

    /**
     * Konstruktor fuer eine Liste mit Regelergebnissen
     * @param rid   Eine RegelID
     * @param result Ein Regelergebnis
     */
    public RuleResultList(int rid, boolean result){

        ruleResults.add(new Result(rid, result));
    }

    /**
     * Konstruktor fuer eine Liste an Regelergebnissen ohne Parameter
     */
    public RuleResultList(){

    }

    /**
     * Einfuegen eines Result Objektes in die Liste
     * @param rid   Die ID einer Regel
     * @param result Das Ergebniss einer Regel
     */
    public void add(int rid, boolean result){

        ruleResults.add(new Result(rid, result));
    }

    /**
     * Gibt eine RgelID zurueck
     * @param index Die Stelle in der Liste an der das Objekt steht von dem die ID zurueck gegeben wird
     * @return  Gibt eine Zahl zurueck (RegelID)
     */
    public int getRID(int index){
        int rid = ruleResults.get(index).getID();
        return rid;
    }

    /**
     * Gibt ein Ergebniss zurueck
     * @param index Die Stelle in der Liste an der das Objekt steht von dem das Ergebnis zurueck gegeben wird
     * @return Einen boolean Wert (Ergebnis)
     */
    public boolean getResult(int index){
        boolean result = ruleResults.get(index).getResult();
        return result;
    }

    /**
     * Gibt ein ResultObjekt zurueck
     * @param index Die Stelle in der Liste an der das Objekt steht das zurueck gegeben wird
     * @return  Ein Result Objekt, welches eine RegelID und ein Ergebnis beinhaltet
     */
    public Result getResultObject(int index){
        Result result = ruleResults.get(index);
        return result;
    }

    /**
     * Gibt die Groesse der Liste zurueck
     * @return Eine Zahl (Groesse der Liste)
     */
    public int getSize(){
        return ruleResults.size();
    }
}

