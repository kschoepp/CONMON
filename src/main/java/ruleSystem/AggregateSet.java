package ruleSystem;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * Created by Kathrin Schoepp on 10.09.2017
 **/

public class AggregateSet {

    List<Aggregate> aggregates = new ArrayList<Aggregate>();

    /**
     * Konstruktor fuer eine Liste an Aggregaten
     * @param aid   Die ID eines Aggregates
     * @param parameter Ein Parameter eines Aggregates
     * @param operation Ein Operator eines Aggregates
     */
    public AggregateSet(int aid, String parameter, String operation){

        aggregates.add(new Aggregate(aid, parameter, operation));
    }

    /**
     * Konstruktor fuer eine Liste von Regeln ohne Parameter
     */
    public AggregateSet(){

    }

    /**
     * Hinzufuegen eines AggregatObjektes mit AggregatID, Parameter und Operator
     * @param aid Die ID eines Aggregates
     * @param parameter Ein Parameter eines Aggregates
     * @param operation Ein Operator eines Aggregates
     */
    public void add(int aid, String parameter, String operation){

        aggregates.add(new Aggregate(aid, parameter, operation));
    }

    /**
     * Gibt einen Parameter eines Aggregats zurueck
     * @param index Die Stelle in der Liste, an der sich das Objekt in der Liste befindet, von dem das Parameter zurueck gegeben wird
     * @return  Einen Parameter eines Aggregates
     */
    public String getParameter(int index){
        String parameter = aggregates.get(index).getParameter();
        return parameter;
    }

    /**
     * Gibt einen Operator einer Regel zurueck
     * @param index Die Stelle in der Liste, an der sich das Objekt befindet, von dem der Operator zurueck gegeben wird
     * @return  Ein Operator eines Aggregates
     */
    public String getOperation(int index){
        String operation = aggregates.get(index).getOperation();
        return operation;
    }

    /**
     * Gibt die ID eines Aggregats zurueck
     * @param index Die Stelle in der List, an der sich das Objekt befindet, von dem die ID zurueck gegeben wird
     * @return Eine Zahl (AggregateID)
     */
    public int  getAid(int index){
        int rid = aggregates.get(index).getAid();
        return rid;
    }

    /**
     * Gibt eien AggregatObjekt zurueck
     * @param index Die Stelle in der Liste, an der sich das Objekt befindet, welches zureuck gegeben wird
     * @return Ein AggregatObjekt
     */
    public Aggregate getAggregateObject(int index){
        Aggregate aggregate = aggregates.get(index);
        return aggregate;
    }

    /**
     * Gibt die Groesse der Liste zureuck
     * @return  Eine Zahl (Die Groesse der Liste)
     */
    public int getSize() {
        return aggregates.size();
    }
}
