package ruleSystem.expressionTree;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Stack;



public class ETree
{
    private Node root = null;
    private ArrayList<String> postfixFormular;
    private ArrayList<String> infixFormular;
    private HashMap<Integer, Double> treemap;
    private int blocksComputed = 0;
    private Object result;
    private boolean build = false;

    /**
     * Konstruktor des Expression Trees
     * @param postfix Postfix Ausdruck in Form einer Arraylist
     * @param infix Infix Ausdruck in Form einer Arraylist
     * @param tm Hashmap mit Sensoren und Sernsorwerten
     */
    public ETree(ArrayList<String> postfix, ArrayList<String> infix, HashMap<Integer, Double> tm)
    {
        setPostfixFormular( postfix );
        setInfixFormular( infix );
        setTreemap( tm );
        
        System.out.println( "--------------------------------" );
        System.out.println( "-> Expression = " + getInfixFormular() );
        System.out.println( "--------------------------------" );
        System.out.println( "" );
        
        buildTree();
    }

    /**
     * Getter und Setter
     */
    private void setInfixFormular( ArrayList<String> infix )
    {
        if ( infix.isEmpty() )
        {
            throw new IllegalArgumentException( "Arraylist must not be empty." );
        }
        
        infixFormular = infix;
    }
    
    public ArrayList<String> getInfixFormular()
    {
        return infixFormular;
    }
    
    private void setPostfixFormular(ArrayList<String> postfix)
    {
        if ( postfix.isEmpty() )
        {
            throw new IllegalArgumentException( "Arraylist must not be empty." );
        }
        
        postfixFormular = postfix;
    }
    
    public ArrayList<String> getPostfixFormular()
    {
        return postfixFormular;
    }

    public void setTreemap ( HashMap<Integer, Double> tm) {
        if ( tm.isEmpty() ) {
            throw new IllegalArgumentException( "Hashmap must not be empty." );
        }

        treemap = tm;
    }

    public HashMap<Integer, Double> getTreemap() {
        return treemap;
    }
    
    public int numBlocksComputed()
    {
        return blocksComputed;
    }    
    
    public void calcResult()
    {
        setResult( root.evaluate() );
    }
    
    private void setResult( Object result )
    {
        this.result = result;
    }
    
    public Object getResult()
    {
        return result;
    }
    
    private void setBuild()
    {
        build = numBlocksComputed() > 0;
    }
    
    public boolean isBuild()
    {
        return build;
    }

    /**
     * Baut und berechnet einen Expression Tree
     */
    public void buildTree()
    {
        Stack<Node> nodes = new Stack<>();
        ArrayList<String> formular = postfixFormular;
        
        for ( int i = 0; i < formular.size(); i++ )
        {
            String element = formular.get( i );
            //DEBUG System.out.println( "____char: " + c );
            
            if ( Operator.isOperator( element ) )
            {
               Node rightNode = nodes.pop();
               Node leftNode = nodes.pop();
               //DEBUG System.out.println( "____push OP-Node: " + Character.toString( c ) );
               nodes.push( new Node( leftNode, rightNode, element ) );
            }
            else
            {
                int sensorID = 0;
                String sid = formular.get( i );
                if(sid.charAt(0) == 's') {
                    sensorID = Integer.parseInt(sid.substring(1));
                    if(treemap.get(sensorID) == null){
                        throw new NoValueException("No value of sensor:"+ sensorID);
                    }
                    nodes.add( new Node( sid, treemap.get(sensorID)));
                } else if(sid.charAt(0) == 'm' || sid.charAt(0) == 'a'){
                    int endIndex = sid.indexOf("(");
                    String s = sid.substring(3,endIndex);
                    sensorID = Integer.parseInt(s);
                    if(treemap.get(sensorID) == null){
                        throw new NoValueInPeriodException("No value of sensor: "+sensorID+"in the period");
                    }
                    nodes.add( new Node( sid, treemap.get(sensorID)));
                } else if(sid.charAt(0) == 'c'){
                    double value = Double.parseDouble(sid.substring(1));
                    nodes.add( new Node( sid, value));
                }


            }
        }
        
        root = nodes.pop();
        calcResult();
        
        display();
        
        setBuild();
        blocksComputed++;
    }

    /**
     * Visualisiert den Expression Tree
     * @return Gibt Darstellung des Expression Trees als String zurück
     */
    public String visualize()
    {
        StringBuffer sb = new StringBuffer( "" );
            sb.append( "-------------------------------- [Block " + numBlocksComputed() + "] \n" );
            sb.append( root.visualize() );
            sb.append( "| \n" );
            sb.append( "└> Result = " + getResult() + "\n" );
            sb.append( "-------------------------------- [Block " + numBlocksComputed() + "] \n" );
       
        return sb.toString();
    }

    /**
     * Gibt den Expression Tree auf der Standartausgabe aus
     */
    public void display()
    {
        System.out.println( visualize() );
    }
    
    private String inOrder( Node curNode )
    {
        StringBuffer sb = new StringBuffer( "" );
        
        if ( curNode != null )
        {
            sb.append( inOrder( curNode.getLeft() ) );
            sb.append( curNode.toString() + " " );
            sb.append( inOrder( curNode.getRight() ) );
        }
        
        return sb.toString();
    }
    
    private String inOrder()
    {
        return inOrder( root );
    }
   
    public String toString()
    {       
        return inOrder();
    }
}