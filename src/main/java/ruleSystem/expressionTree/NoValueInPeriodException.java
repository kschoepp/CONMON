package ruleSystem.expressionTree;

/**
 *     _____
 *    |_   _|
 *      | |  _ __   __ _  ___ _ __   ___ ___
 *      | | | '_ \ / _` |/ _ \ '_ \ / __/ _ \
 *     _| |_| | | | (_| |  __/ | | | (_|  __/
 *    |_____|_| |_|\__, |\___|_| |_|\___\___|
 *                  __/ |
 *                 |___/
 *    https://ingence.de
 *
 * Created by Kathrin Schoepp on 15.09.2017
 **/

public class NoValueInPeriodException extends RuntimeException {
    public NoValueInPeriodException() {
        super();
    }

    public NoValueInPeriodException( String msg ) {
        super( msg );
    }

    public NoValueInPeriodException( Throwable cause ) {
        super( cause );
    }

    public NoValueInPeriodException( String msg, Throwable cause ) {
        super( msg, cause );
    }
}
