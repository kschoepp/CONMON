package ruleSystem.expressionTree;

/**
 *     _____
 *    |_   _|
 *      | |  _ __   __ _  ___ _ __   ___ ___
 *      | | | '_ \ / _` |/ _ \ '_ \ / __/ _ \
 *     _| |_| | | | (_| |  __/ | | | (_|  __/
 *    |_____|_| |_|\__, |\___|_| |_|\___\___|
 *                  __/ |
 *                 |___/
 *    https://ingence.de
 *
 * Created by Kathrin Schoepp on 15.09.2017
 **/

public class NoFormularGivenException extends RuntimeException
{
    public NoFormularGivenException() {
        super();
    }

    public NoFormularGivenException( String msg ) {
        super( msg );
    }

    public NoFormularGivenException( Throwable cause ) {
        super( cause );
    }

    public NoFormularGivenException( String msg, Throwable cause ) {
        super( msg, cause );
    }
}