package ruleSystem.expressionTree;



public class Node
{
    public static final int TYPE_NONE = 0;
    public static final int TYPE_OP = 1;
    public static final int TYPE_VAL = 2;

    private Node left = null;
    private Node right = null;
    private String data = null;
    private Object value = 0;
    private int type = TYPE_NONE;

    /**
     * Konstruktor für Value-Knotens
     * @param data Bezeichnung des Knotens als String
     * @param value Wert des Knotens als Double
     */
    public Node( String data, Object value )
    {
        this( null, null, data, (double) value, TYPE_VAL );
    }

    /**
     * Konstruktor für Operator-Knoten
     * @param left linker Sohn des Knotens
     * @param right  rechter Sohn des Knotens
     * @param data Art des Operator-Knotens als String
     */
    public Node( Node left, Node right, String data )
    {
        this( left, right, data, 0, TYPE_OP );
    }

    /**
     * Standartkonstruktor
     * @param left linker Sohn des Knotens
     * @param right rechter Sohn des Knotens
     * @param data Bezeichnung des Knotens als String
     * @param value Wert des Knotens Double
     * @param type Typ des Knotens
     */
    private Node( Node left, Node right, String data, double value, int type )
    {
        setLeft( left );
        setRight( right );
        setData( data );
        setValue( value );
        setType( type );
    }

    /**
     * Getter und Setter
     */
    private void setLeft( Node left )
    {
        this.left = left;
    }
        
    public Node getLeft()
    {
        return left;
    }
    
    private void setRight( Node right )
    {
        this.right = right;
    }
    
    public Node getRight()
    {
        return right;
    }
    
    public void setData( String data )
    {
        if ( data.trim().equals( "" ) )
        {
            throw new MalformedNodeException( "String passed is empty." );
        }
        
        this.data = data;
    }  
    
    public String getData()
    {
        return data;
    }
    
    private String getOperator()
    {
        return getData();
    }
    
    public void setValue( Object value )
    {
        this.value = value;
    }  
    
    public Object getValue()
    {
        return value;
    }
    
    private void setType( int type )
    {
        if ( !( type == TYPE_NONE || type == TYPE_OP || type == TYPE_VAL ) )
        {
            throw new MalformedNodeException( "Invalid Node Type." );
        }
        
        this.type = type;
    }
    
    public int getType()
    {
        return type;
    }


    /**
     * Berechnet den Wert eines Knotens rekursiv.
     * @return Gibt Wert des Knotens zurück
     */
    public Object evaluate()
    {
        if ( getType() == TYPE_OP )
        {
            setValue((Operator.compute( getLeft().evaluate(), getOperator(), getRight().evaluate() )) );
        }

        //DEBUG System.out.println( this.toString() );

        return getValue();
    }

    /**
     * Zeigt den Knoten selbst und mögliche Kinder an
     * @param depth Tiefe des Baumes
     * @return Gibt Knoten und mögliche Kinder als String zurück
     */
    private String visualize( int depth )
    {
        String tab = "|  ";
        StringBuffer sb = new StringBuffer( "" );
        
        if ( this.getType() == Node.TYPE_VAL )
        {
            for ( int i = 0; i < depth; i++ )
            {
                sb.append( tab );
            }
            
            sb.append( "+--" + this + "\n" );
        }
        else
        {
            sb.append( this.getRight().visualize( depth + 1 ) );
            
            for ( int i = 0; i < depth; i++ )
            {
                sb.append( tab );
            }
            
            sb.append( "+--" + this + "\n" );
            sb.append( this.getLeft().visualize( depth + 1 ) );
        }
        
        return sb.toString();
    }
    
    public String visualize()
    {
        return visualize( 0 );
    }

    /**
     * Gibt den String, den man bei der Funktion "visualize()" erhält auf der Standartausgabe aus
     */
    public void display()
    {
        System.out.print( visualize() );
    }


    public String toString()
    {
        StringBuffer sb = new StringBuffer( "" );
        
        if ( getType() == TYPE_NONE  )
        {
            sb.append( "{ }" );
        }
        else if ( getType() == TYPE_OP )
        {
            sb.append( "[" ).append( getOperator() ).append( ":" ).append( getValue() ).append( "]" );
        }
        else if ( getType() == TYPE_VAL )
        {
            sb.append( "{" ).append( getData() ).append( "::" ).append( getValue() ).append( "}" );
        }
        else
        {
            sb.append( "{-Invalid Node-}" );
        }
        
        return sb.toString();
    }
}