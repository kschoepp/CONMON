package ruleSystem.expressionTree;

import databaseUtils.DatabaseConnector;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Queue;



public class FormularCalculator {

    /**
     * Erstellt eine Hashmap anhand der mit einer Queue übergebenen Sensoren oder Funktionen und deren Werten, welche aus der Datenbank geholt werden
     * @param ruleQueue Queue mit Sensoren und Operatoren
     * @return Gibt eine Hashmap mit Sernsoren inklusive Werten zurueck
     * @throws Exception
     */
    public static HashMap<Integer, Double> getHashMap(Queue<String> ruleQueue) throws Exception{
        HashMap<Integer, Double> treeMap = new HashMap<>();
        Iterator<String> ruleIt = ruleQueue.iterator();
        while(ruleIt.hasNext()) {
            String parameter = ruleIt.next();
            Character parameterFirst = parameter.charAt(0);

            if (parameterFirst.equals('s')) {
                String parameterSecond = parameter.substring(1);
                int sid = Integer.parseInt(parameterSecond);
                DatabaseConnector dc = new DatabaseConnector();
                dc.connect();
                String v = dc.getSensorValue(sid);
                if(v.equals("undefinied")){
                    return null;
                }
                double value = Double.parseDouble(v);
                dc.close();
                treeMap.put(sid, value);
            } else if (parameterFirst.equals('a')){
                int startIndex = parameter.indexOf("(");
                int endIndex = parameter.indexOf(")");
                String period = parameter.substring(startIndex+1, endIndex);
                String newParameter = parameter.substring(0,startIndex);
                String id = newParameter.substring(3);
                int sid = Integer.parseInt(id);
                DatabaseConnector dc = new DatabaseConnector();
                dc.connect();
                double value = dc.getSensorAvg(sid,period);
                dc.close();
                treeMap.put(sid, value);
            }else if (parameterFirst.equals('m')){
                if(parameter.substring(0,2).equals("ma")){
                    int startIndex = parameter.indexOf("(");
                    int endIndex = parameter.indexOf(")");
                    String period = parameter.substring(startIndex+1, endIndex);
                    String newParameter = parameter.substring(0,startIndex);
                    String id = newParameter.substring(3);
                    int sid = Integer.parseInt(id);
                    DatabaseConnector dc = new DatabaseConnector();
                    dc.connect();
                    double value = dc.getSensorMax(sid, period);
                    dc.close();
                    treeMap.put(sid, value);
                }else if(parameter.substring(0,2).equals("mi")){
                    int startIndex = parameter.indexOf("(");
                    int endIndex = parameter.indexOf(")");
                    String period = parameter.substring(startIndex+1, endIndex);
                    String newParameter = parameter.substring(0,startIndex);
                    String id = newParameter.substring(3);
                    int sid = Integer.parseInt(id);
                    DatabaseConnector dc = new DatabaseConnector();
                    dc.connect();
                    double value = dc.getSensorMin(sid, period);
                    dc.close();
                    treeMap.put(sid, value);
                } else if (parameter.substring(0,2).equals("me")){
                    int startIndex = parameter.indexOf("(");
                    int endIndex = parameter.indexOf(")");
                    String period = parameter.substring(startIndex+1, endIndex);
                    String newParameter = parameter.substring(0,startIndex);
                    String id = newParameter.substring(3);
                    int sid = Integer.parseInt(id);
                    DatabaseConnector dc = new DatabaseConnector();
                    dc.connect();
                    double value = dc.getSensorMed(sid, period);
                    dc.close();
                    treeMap.put(sid, value);
                }
            }else if (parameterFirst.equals('c')){
                String v = parameter.substring(1);
                double value = Double.parseDouble(v);
                treeMap.put(null,value);
            }

        }
        return treeMap;
    }

    /**
     * Erstellt anhand einer übergebenen Queue eine Arraylist
     * @param ruleQueue Queue mit Sensoren und Operatoren
     * @return Gibt einen Infix Ausdruck in Form einer Arraylist zurück
     */
    public static ArrayList<String> getArrayList(Queue<String> ruleQueue){
        ArrayList<String> rule = new ArrayList<>();
        int max = ruleQueue.size();
        for(int i = 0; i < max; i++){
            String partOfRule = ruleQueue.poll();
            rule.add(partOfRule);
        }
        return rule;
    }

    /**
     * Startet das umwandeln eines Infix Ausdrucks in einen Postfix Ausdruck
     * @param rule Infix Ausruck in Form einer Arraylist
     * @return Gibt einen Postfix Ausdruck in Form einer Arraylist zurück
     */
    public static ArrayList<String> verifyFormular( ArrayList<String> rule ) {
        InfixConverter ic = new InfixConverter( rule );
        return ic.getPostfix();
    }
}
