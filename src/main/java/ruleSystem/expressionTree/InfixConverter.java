package ruleSystem.expressionTree;

import java.util.ArrayList;
import java.util.Stack;



public class InfixConverter
{

    private ArrayList<String> infix = new ArrayList<>();
    private ArrayList<String> postfix;
    private Stack<String> opStack = null;

    /**
     * Konstruktor der Klasse InfixConverter
     * @param formular Auszuwertender Ausdruck als Arraylist
     */
    public InfixConverter( ArrayList<String> formular)
    {
        setInfix( formular );
    }

    /**
     * Getter und Setter für die Arraylist infix
     */
    public ArrayList<String> getInfix()
    {
        return infix;
    }

    public void setInfix( ArrayList<String> formular )
    {
        for ( int i = 0; i < formular.size(); i++ ) {

            if (formular.get(i).trim().equals("")) {
                throw new NoFormularGivenException("There is no formular to parse.");
            }
        }

        infix = verifyFormular( formular );
    }

    // Verfifies a formular, and returns a trimmed version of it

    /**
     * Prüft ob der übergebene Ausdruck gültig ist und schneidet unnötige Zeichen weg
     * @param formular Arraylist mit einem auszuwertenden Ausdruck
     * @return Gibt eine gekürzte und korrekte Form eines Infix Ausdrucks zurück
     * @throws MalformedFormularException
     */
    public static final ArrayList<String> verifyFormular( ArrayList<String> formular )
    {
        // Check for misplaced operators and brackets
        for ( int i = 0; i < formular.size(); i++ ) {

            String element = formular.get(i);
            element.replaceAll("\\s+", "");
            formular.set( i, element);

        }

        int len = formular.size();
        int bracketBalance = 0;

        for ( int i = 0; i < len; i++ )
        {
            // Check if all operators and brackets are placed correctly are placed correctly
            if ( i == 0  || i == len - 1 )
            {
                // Is there an operator at the beginning or the end of the formular?
                if ( Operator.isOperator(  formular.get( i ) ) )
                {
                    throw new MalformedFormularException( "Operators are not allowed to be placed at the start or the end of a formular." );
                }

                // Check if the brackets are placed correctly at the beginning or end of the fomrular
                if ( ( i == 0 && Operator.isRightBracket(  formular.get( 0 ) ) ) || ( i == len - 1 && Operator.isLeftBracket( formular.get( len - 1 ) ) ) )
                {
                    throw new MalformedFormularException( "Brackets at the beginning or the end of the formular are wrong." );
                }
            }

            // Check if there are any operators directly followed by other operators, or if there are illegal operations with brackets [ "(OP", "OP)" ]
            if ( Operator.isOperator(  formular.get( i ) ) && Operator.isOperator( formular.get( i + 1 ) ) )
            {
                throw new MalformedFormularException( "There mustn't be more than one operator in a row." );
            }
            else if ( Operator.isLeftBracket(  formular.get( i ) ) && Operator.isOperator(  formular.get( i + 1 ) ) )
            {
                throw new MalformedFormularException( "A left parenthesis can't be followed by an operator." );
            }
            else if ( Operator.isOperator( formular.get( i ) ) && Operator.isRightBracket( formular.get( i +1 ) ) )
            {
                throw new MalformedFormularException( "A right parenthesis can't be preceded by an operator." );
            }

            // Check if all brackets are in pairs
            if ( Operator.isLeftBracket( formular.get( i ) ) )
            {
                bracketBalance++;
            }

            if ( Operator.isRightBracket( formular.get( i ) ) )
            {
                bracketBalance--;
            }
        }

        if ( bracketBalance < 0 )
        {
            throw new MalformedFormularException( "Not all brackets are opened or closed correctly (More closed than opened)." );
        }

        if ( bracketBalance > 0 )
        {
            throw new MalformedFormularException( "Not all brackets are opened or closed correctly (More opened than closed)." );
        }

        return formular;
    }

    /**
     * Konvertiert einen Infix Ausdruck in einen Postfix Ausdruck
     * @return Gibt den Postfix Ausdruck zurück
     */
    public ArrayList<String> getPostfix()
    {
        //exceptions
        for ( int i = 0; i < infix.size(); i++ ) {

            if (infix.get(i).trim().equals("")) {
                throw new NoFormularGivenException("There is no formular to parse.");
            }

        }

        //main code
        for ( int i = 0; i < infix.size(); i++ ) {

            String element = infix.get(i);
            element.replaceAll( "\\s+", "" );
            infix.set( i, element );

        }
        opStack = new Stack<>();
        postfix = new ArrayList<String>();

        for ( int i = 0; i < infix.size(); i++ )
        {
            String element = infix.get( i );

            if ( !Operator.isOperator( element ) )
            {
                postfix.add( element );
            }
            else
            {
                //insert a delimiter, so that variable names can be seperated in postfix
                if ( Operator.isLeftBracket( element ) )
                {
                    opStack.push( element );
                }
                else if ( Operator.isRightBracket( element ) )
                {
                    while ( !opStack.empty() && opStack.peek() != Operator.LEFT_BRACKET )
                    {
                        postfix.add( opStack.pop() );
                    }

                    if ( !opStack.empty() )
                    {
                        opStack.pop();
                    }
                }
                else
                {
                    while ( !opStack.empty() && Operator.getPriority( element ) <= Operator.getPriority( opStack.peek() ) )
                    {
                        postfix.add( opStack.pop() );
                    }

                    opStack.push( element );
                }
            }
        }

        while ( !opStack.empty() )
        {
            postfix.add( opStack.pop() );
        }

        return postfix;
    }
}