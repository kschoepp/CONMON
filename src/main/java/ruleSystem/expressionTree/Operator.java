package ruleSystem.expressionTree;



public class Operator
{
    /**
     * Operatoren als Konstanten
     */
    public static final String PLUS = "+";
    public static final String MINUS = "-";
    public static final String MUL = "*";
    public static final String DIV = "/";
    public static final String POWER = "^";
    public static final String LEFT_BRACKET = "(";
    public static final String RIGHT_BRACKET = ")";
    public static final String LEFT_BRACKET_SQUARE = "[";
    public static final String RIGHT_BRACKET_SQUARE = "]";
    public static final String LEFT_BRACKET_CURLY = "{";
    public static final String RIGHT_BRACKET_CURLY = "}";
    public static final String LESS = "<";
    public static final String GREATER = ">";
    public static final String LESS_EQUAL = "<=";
    public static final String GREATER_EQUAL = ">=";
    public static final String EQUAL = "==";
    public static final String NOT_EQUAL = "!=";
    public static final String AND = "&&";
    public static final String OR = "||";
    public static final String NEGATION = "!";


    /**
     * Prioritäten der Operatoren von niedrig zu hoch als Konstanten
     */
    private static final int PRIORITY_NONE = -1;
    private static final int PRIORITY_BRACKETS = 1;

    private static final int PRIORITY_COMPARE = 4;
    private static final int PRIORITY_NEGATION = 3;
    private static final int PRIORITY_NEXUS = 2;

    private static final int PRIORITY_LINEOP = 7;
    private static final int PRIORITY_POINTOP = 6;
    private static final int PRIORITY_POWER = 5;


    public static String getPlus()
    {
        return PLUS;
    }
    
    public static boolean isPlus( String c )
    {
        return c.equals( getPlus() );
    }
    
    public static double computePlus( double leftOperand, double rightOperand )
    {
        return leftOperand + rightOperand;
    }
    
    // Minus
    public static String getMinus()
    {
        return MINUS;
    }
    
    public static boolean isMinus( String  c )
    {
        return c.equals( getMinus() );
    }
    
    public static double computeMinus( double leftOperand, double rightOperand )
    {
        return leftOperand - rightOperand;
    }
    
    // Line Operator
    public static boolean isLineOperator( String  c )
    {
        return isPlus( c ) || isMinus( c );
    }
    
    // -Mul & Div
    // Mul
    public static String  getMul()
    {
        return MUL;
    }
    
    public static boolean isMul( String  c )
    {
        return c.equals( getMul() );
    }
    
    public static double computeMul( double leftOperand, double rightOperand )
    {
        return leftOperand * rightOperand;
    }
    
    // Div
    public static String  getDiv()
    {
        return DIV;
    }
    
    public static boolean isDiv( String  c )
    {
        return c.equals ( getDiv() );
    }
    
    public static double computeDiv( double leftOperand, double rightOperand )
    {
        return leftOperand / rightOperand;
    }
    
    // Point Operator
    public static boolean isPointOperator( String c )
    {
        return isMul( c ) || isDiv( c );
    }
    
    // -Power
    public static String  getPower()
    {
        return POWER;
    }
    
    public static boolean isPower( String  c )
    {
        return c.equals( getPower() );
    }
    
    public static double computePower( double leftOperand, double rightOperand )
    {
        return Math.pow( leftOperand, rightOperand );
    }

    // -Compare Operator
    // Less
    public static String getLess() {
        return LESS;
    }

    public static boolean isLess( String c ) {
        return c.equals( getLess() );
    }

    public static boolean computeLess( double leftOperand, double rightOperand ) {
        return leftOperand < rightOperand;
    }

    // Greater
    public static String getGreater() {
        return GREATER;
    }

    public static boolean isGreater( String c ) {
        return c.equals( getGreater() );
    }

    public static boolean computeGreater( double leftOperand, double rightOperand ) {
        return leftOperand > rightOperand;
    }

    // LessEqual
    public static String getLessEqual() {
        return LESS_EQUAL;
    }

    public static boolean isLessEqual( String c ) {
        return c.equals( getLessEqual() );
    }

    public static boolean computeLessEqual( double leftOperand, double rightOperand ) {
        return leftOperand <= rightOperand;
    }

    // GreaterEqual
    public static String getGreaterEqual() {
        return GREATER_EQUAL;
    }

    public static boolean isGreaterEqual( String c ) {
        return c.equals( getGreaterEqual() );
    }

    public static boolean computeGreaterEqual( double leftOperand, double rightOperand ) {
        return leftOperand == rightOperand;
    }

    // Equal
    public static String getNotEqual() {
        return NOT_EQUAL;
    }

    public static boolean isNotEqual( String c ) {
        return c.equals( getNotEqual() );
    }

    public static boolean computeNotEqual( double leftOperand, double rightOperand ) {
        return leftOperand > rightOperand;
    }

    // Not Equal
    public static String getEqual() {
        return EQUAL;
    }

    public static boolean isEqual( String c ) {
        return c.equals( getEqual() );
    }

    public static boolean computeEqual( double leftOperand, double rightOperand ) {
        return leftOperand > rightOperand;
    }

    // -Nexus
    // And
    public static String getAnd() {
        return AND;
    }

    public static boolean isAnd( String c ) {
        return c.equals( getAnd() );
    }

    public static boolean computeAnd( boolean leftOperand, boolean rightOperand ) { return leftOperand && rightOperand; };

    // Or
    public static String getOr() {
        return OR;
    }

    public static boolean isOr( String c ) {
        return c.equals( getOr() );
    }

    public static boolean computeOr( boolean leftOperand, boolean rightOperand ) { return leftOperand || rightOperand; };

    // Negation
    public static String getNegation() {
        return NEGATION;
    }

    public static boolean isNegation( String c ) {
        return ( c.equals(getNegation()) );
    }

    public static  boolean computeNegation( boolean rightOperand ) {
        return !rightOperand;
    }

    // -Is Operator

    public static  boolean isOperator ( String c ) {
        return ( isOperatorArithmetic( c ) || isOperatorLogic( c ) || isOperatorNexus( c ) );
    }

    public static boolean isOperatorArithmetic( String  c )
    {
        return (  isPlus( c ) || isMinus( c )
                || isMul( c ) || isDiv( c )
                || isPower( c ) );
    }

    public static boolean isOperatorLogic( String c ) {
        return ( isGreater( c )
                || isLess( c ) || isGreaterEqual( c )
                || isLessEqual( c ) || isEqual( c )
                || isNotEqual( c ) );
    }

    public static boolean isOperatorNexus( String c ) {
        return ( isAnd( c ) || isOr( c ) || isNegation( c ) );
    }
    
    // --Brackets
    // -Brackes
    public static String getLeftBracket()
    {
        return LEFT_BRACKET;
    }
    
    public static boolean isLeftBracket( String  c )
    {
        return c.equals( getLeftBracket() );
    }
    
    public static String  getRightBracket()
    {
        return RIGHT_BRACKET;
    }
    
    public static boolean isRightBracket( String  c )
    {
        return c.equals( getRightBracket() );
    }
    
    // -Square Brackets
    public static String getLeftSquareBracket()
    {
        return LEFT_BRACKET_SQUARE;
    }
    
    public static boolean isLeftSquareBracket( String  c )
    {
        return c.equals( getLeftSquareBracket() );
    }
    
    public static String  getRightSquareBracket()
    {
        return RIGHT_BRACKET_SQUARE;
    }
    
    public static boolean isRightSquareBracket( String c )
    {
        return c.equals( getRightSquareBracket() );
    }
    
    // -Curly Brackets
    public static String  getLeftCurlyBracket()
    {
        return LEFT_BRACKET_CURLY;
    }
    
    public static boolean isLeftCurlyBracket( String  c )
    {
        return c.equals( getLeftCurlyBracket() );
    }
    
    public static String  getRightCurlyBracket()
    {
        return RIGHT_BRACKET_CURLY;
    }
    
    public static boolean isRightCurlyBracket( String c )
    {
        return c.equals( getRightCurlyBracket() );
    }

    // -Is Bracket
    public static boolean isBracket( String  c )
    {
        return (   isLeftBracket( c ) || isRightBracket( c )
                || isLeftSquareBracket( c ) || isRightSquareBracket( c )
                || isLeftCurlyBracket( c ) || isRightCurlyBracket( c ));
    }

    /**
     * Funktionen zur Zurückgabe der Prioritäten
     */
    public static int getNonePriority()
    {
        return PRIORITY_NONE;
    }
    
    public static int getBracketPriority()
    {
        return PRIORITY_BRACKETS;
    }
    
    public static int getLineOperatorPriority()
    {
        return PRIORITY_LINEOP;
    }
    
    public static int getPointOperatorPriority()
    {
        return PRIORITY_POINTOP;
    }
    
    public static int getPowerPriority()
    {
        return PRIORITY_POWER;
    }

    public static int getCompareLogicPriority() {
        return PRIORITY_COMPARE;
    }

    public static  int getNegationPriority() {
        return PRIORITY_NEGATION;
    }

    public static int getCompareNexusPriority() {
        return PRIORITY_NEXUS;
    }

    /**
     * Entscheidet anhand der jeweiligen Operandendatentypen welche Unterfunktion zum berechnen der Ergebnisses benutzt wird
     * @param leftOperand linker Operand als Object
     * @param op Operator als String
     * @param rightOperand rechter Operand als Object
     * @return Gibt das Ergebnis der jeweiligen Unterfunktion als Object zurück
     */
    public static Object compute (Object leftOperand, String op, Object rightOperand) {

        if( leftOperand instanceof Double && rightOperand instanceof Double && isOperatorArithmetic( op ) ) {
            return (  (double)(computeArithmetic( (double)leftOperand, op, (double)rightOperand)) );
        }
        else if ( leftOperand instanceof Double && rightOperand instanceof Double && isOperatorLogic( op ) ) {
            return ( (boolean) (computeLogic( (double)leftOperand, op, (double)rightOperand )) );
        }
        else if ( leftOperand instanceof Boolean && rightOperand instanceof Boolean && isOperatorNexus( op ) ) {
            return ( (boolean) computeNexus( (boolean)leftOperand, op, (boolean)rightOperand) );
        }
        else throw new IllegalArgumentException( "Error at Operator '" + op + "'." );
    }

    /**
     * Berechnet Arithmetische Verknüpfungen
     * @param leftOperand linker Operand als Double
     * @param op Operator als String
     * @param rightOperand rechter Operand als Double
     * @return Gibt das Ergebnis der Berechnung als Double zurück
     * @throws IllegalArgumentException
     */
    public static double computeArithmetic( double leftOperand, String  op, double rightOperand )
    {
        // Exceptions
        if ( !isOperator( op ) )
        {
            throw new IllegalArgumentException( "Operator '" + op + "' not declared." );
        }
        
        // Main code
        double result = 0;
        
        if ( isPlus( op ) )
        {
            result = computePlus( leftOperand, rightOperand );
        }
        else if ( isMinus( op ) )
        {
            result = computeMinus( leftOperand, rightOperand );
        }               
        else if ( isMul( op ) )
        {
            result = computeMul( leftOperand, rightOperand );
        }
        else if ( isDiv( op ) )
        {
            result = computeDiv( leftOperand, rightOperand );  
        }
        else if ( isPower( op ) )
        {
            result = computePower( leftOperand, rightOperand );
        }

        return result;
    }

    /**
     * Berechnet logische Vergleiche
     * @param leftOperand linker Operand als Double
     * @param op Operator als String
     * @param rightOperand rechter Operand als Double
     * @return Gibt das Ergebnis des Vergleichs als Boolean zurück
     * @throws IllegalArgumentException
     */
    public static boolean computeLogic( double leftOperand, String  op, double rightOperand ) {

        if ( !isOperatorLogic( op ) )
        {
            throw new IllegalArgumentException( "Operator '" + op + "' not declared." );
        }

        // Main code
        boolean result = false;

        if ( isGreater( op ) )
        {
            result = computeGreater( leftOperand, rightOperand );
        }
        else if ( isLess( op ) )
        {
            result = computeLess( leftOperand, rightOperand );
        }
        else if ( isGreaterEqual( op ) )
        {
            result = computeGreaterEqual( leftOperand, rightOperand );
        }
        else if ( isLessEqual( op ) )
        {
            result = computeLessEqual( leftOperand, rightOperand );
        }
        else if ( isEqual( op ) )
        {
            result = computeEqual( leftOperand, rightOperand );
        }
        else if ( isNotEqual( op ) )
        {
            result = computeNotEqual( leftOperand, rightOperand );
        }

        return result;
    }

    /**
     * Berechnet logische Verknüpfungen
     * @param leftOperand linker Operand als Boolean
     * @param op Operator als String
     * @param rightOperand rechter Operand als Boolean
     * @return Gibt das Ergebnis der Verknüpfung als Boolean zurück
     * @throws IllegalArgumentException
     */
    public static boolean computeNexus ( boolean leftOperand, String op, boolean rightOperand ) {

        if ( !isOperatorNexus( op ) )
        {
            throw new IllegalArgumentException( "Operator '" + op + "' not declared." );
        }

        // Main code
        boolean result = false;;

        if ( isAnd( op ) )
        {
            result = computeAnd( leftOperand, rightOperand );
        }
        else if ( isOr( op ) )
        {
            result = computeOr( leftOperand, rightOperand );
        }
        else if ( isNegation( op ) )
        {
            result = computeNegation( rightOperand );
        }

        return result;
    }

    /**
     * Gibt die Priorität eines Operators zurück.
     * @param op Der zu prüfende Operator
     * @return Gibt die Priorität der übergebenen Operators zurück
     * @throws IllegalArgumentException
     */
    public static int getPriority( String  op )
    {
        // Exceptions
        if ( !( isOperator( op ) || isBracket( op ) ) )
        {
            throw new IllegalArgumentException( "Character passed is no valid operator." );
        }
        
        // Main code
        int priority = getNonePriority();
        
        if ( isBracket( op ) )
        {
            priority = getBracketPriority();
        }
        else if ( isPlus( op ) || isMinus( op ) )
        {
            priority = getLineOperatorPriority();
        }
        else if ( isMul( op ) || isDiv( op ) )
        {
            priority = getPointOperatorPriority();
        }
        else if ( isPower( op ) ) {
            priority = getPowerPriority();
        }
        else if ( isGreater( op ) ) {
            priority = getCompareLogicPriority();
        }
        else if ( isLess( op ) ) {
            priority = getCompareLogicPriority();
        }
        else if ( isGreaterEqual( op ) ) {
            priority = getCompareLogicPriority();
        }
        else if ( isLessEqual( op ) ) {
            priority = getCompareLogicPriority();
        }
        else if ( isEqual( op ) ) {
            priority = getCompareLogicPriority();
        }
        else if ( isNotEqual( op ) ) {
            priority = getCompareLogicPriority();
        }
        else if ( isAnd( op ) ) {
            priority = getCompareNexusPriority();
        }
        else if ( isOr( op ) ) {
            priority = getCompareNexusPriority();
        }
        else if ( isNegation( op ) ) {
            priority = getNegationPriority();
        }

        
        return priority;
    }
}