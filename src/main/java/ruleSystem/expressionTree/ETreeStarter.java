package ruleSystem.expressionTree;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Queue;



public class ETreeStarter {

    private static Object result;

    private static ArrayList<String> infix = new ArrayList<>();
    private static ArrayList<String> postfix = new ArrayList<>();
    private static HashMap<Integer, Double> hm = new HashMap<>();

    /**
     * Startet den Aufbaue und das Berechnen des Expression Trees
     * @param q Queue mit Sensoren und Operatoren
     * @return Ergebnis der Berechnung
     */
    public static Object startETree(Queue<String> q) {
        FormularCalculator fc = new FormularCalculator();
        try {
            hm = fc.getHashMap(q);
            infix = fc.getArrayList(q);
            postfix = fc.verifyFormular(infix);

            ETree tree = new ETree(postfix, infix, hm);
            result = tree.getResult();
        } catch (Exception e) {
            System.out.println(e);
        }

        return result;
    }
}
