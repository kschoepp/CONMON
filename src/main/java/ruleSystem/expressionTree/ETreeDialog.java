package ruleSystem.expressionTree;

import java.util.*;
import java.io.*;

public class ETreeDialog
{
//    //variables
//    private Scanner input = null;
//    private ETree tree = null;
//
//    public ETreeDialog()
//    {
//        input = new Scanner( System.in );
//    }



    public void start() {

        int rid = 2003;
        String s = "R1 && R2 || s1005 < s2003 #";
        String rname = "Rule" + rid;
        String [] arr = s.split(" ");
        System.out.println("Geteilt an jedem Leerzeichen:");
        for (int i = 0; i < arr.length; i = i + 2) {
            System.out.println(arr[i] + " " + arr[i+1]);
        }
        System.out.println(rname);

//        ArrayList<String> infix = new ArrayList<>();
//        infix.add("s001");
//        infix.add("<");
//        infix.add("s002");
//        infix.add("&&");
//        infix.add("s003");
//        infix.add(">");
//        infix.add("s004");
//
//
//        boolean check;
//        check = infix.isEmpty();
//        System.out.println(check);
//        System.out.println();
//        String element;
////        element = infix.get(0);
//        for (int i = 0; i < infix.size(); i++) {
//            System.out.println(infix.get(i));
//        }
//        System.out.println();
//
//        InfixConverter ic = new InfixConverter(infix);
//        ArrayList<String> postfix = ic.getPostfix();
//
//        check = postfix.isEmpty();
//        System.out.println(check);
//        System.out.println();
//        for (int i = 0; i < postfix.size(); i++) {
//            System.out.println(postfix.get(i));
//        }
//        System.out.println();
//
//        HashMap<Integer, Double> hm = new HashMap<>();
//        hm.put(001, 4.0);
//        hm.put(003, 2.0);
//        hm.put(002, 2.0);
//        hm.put(004, 1.0);
//
//        check = hm.isEmpty();
//        System.out.println(check);
//        System.out.println();
//
//        ETree tree = new ETree(postfix, infix, hm);
//        tree.display();
    }
//    public void start()
//    {
//        System.out.println( "Please enter a filename: " );
//        String filename = input.next();
//
//        start( filename );
//    }
    
//    public void start( String filename )
//    {
//        readFromFile( filename );
//    }
//
//    private void readFromFile( String filename )
//    {
//        File tempFile = null;
//
//        try
//        {
//            //exceptions
//            tempFile = new File( filename );
//
//            if ( !tempFile.exists() )
//            {
//                throw new FileDoesNotExistException( "File '" + filename + "' doesn't exist." );
//            }
//
//            if ( tempFile.getName().lastIndexOf( "." ) != -1 && tempFile.getName().lastIndexOf( "." ) != 0 )
//            {
//                throw new FileHasExtensionException( "File '" + filename + "' has an extension, which is not allowed." );
//            }
//
//            if ( !tempFile.canRead() )
//            {
//                throw new FileNotReadableException( "You have no permission to read file '" + filename + "'." );
//            }
//
//            //main code
//            ETreeParser parser = new ETreeParser( filename );
//            tree = parser.getTree();
//
//            System.out.println( ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" );
//
//            if ( tree != null && tree.isBuild() )
//            {
//                System.out.println( "Formular = " + tree.getInfixFormular() );
//                System.out.println();
//                System.out.println( "Key/Value pairs:" );
//
//                for ( int i = 0; i < tree.getNumVariables(); i++ )
//                {
//                    System.out.println( "\t" + tree.getVariables()[i] + " = " + tree.getValues()[i] );
//                }
//                System.out.println();
//                System.out.println( "Result = " + tree.getResult() );
//                System.out.println();
//                System.out.println( "InOrder of last Tree: " + tree.toString() );
//            }
//            else
//            {
//                System.out.println( "Tree wasn't build completely." );
//            }
//
//            System.out.println( ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" );
//        }
//        catch( FileDoesNotExistException e )
//        {
//            System.out.println( e );
//            e.printStackTrace();
//        }
//        catch( FileHasExtensionException e )
//        {
//            System.out.println( e );
//            e.printStackTrace();
//        }
//        catch( FileNotReadableException e )
//        {
//            System.out.println( e );
//            e.printStackTrace();
//        }
//        catch( IllegalArgumentException e )
//        {
//            System.out.println( e );
//            e.printStackTrace();
//        }
//        catch( RuntimeException e )
//        {
//            System.out.println( e );
//            e.printStackTrace();
//        }
//        catch( Exception e )
//        {
//            System.out.println( e );
//            e.printStackTrace();
//        }
//    }
    
    public static void main( String[] args )
    {
        new ETreeDialog().start();
    }
}