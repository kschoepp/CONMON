package ruleSystem;

/**
 *
 * Created by Kathrin Schoepp on 16.09.2017
 **/


public class NoRuleForSensorException extends RuntimeException {
    public NoRuleForSensorException() {
        super();
    }

    public NoRuleForSensorException( String msg ) {
        super( msg );
    }

    public NoRuleForSensorException( Throwable cause ) {
        super( cause );
    }

    public NoRuleForSensorException( String msg, Throwable cause ) {
        super( msg, cause );
    }
}
