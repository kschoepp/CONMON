package ruleSystem;

/**
 *
 * Created by Kathrin Schoepp on 10.09.2017
 **/

public class Aggregate {
    private int aid;
    private String parameter;
    private String operation;
    private int subAid;

    /**
     * Konstruktor fuer ein Aggregat mit zusätzlicher SubaggregatID
     * @param aid Die ID des Aggregates
     * @param parameter Ein Parameter des Aggregates
     * @param operation Eine Operation eines Aggregaates
     * @param subAid Eine UnterID eines Aggregates
     */
    public Aggregate(int aid, String parameter, String operation, int subAid) {
        this.aid = aid;
        this.parameter = parameter;
        this.operation = operation;
        this.subAid = subAid;
    }

    /**
     * Konstruktor fuer ein Aggregat
     * @param aid  Die ID eines Aggregates
     * @param parameter Ein Parameter eines Aggregates
     * @param operation Ein Operator eines Aggregates
     */
    public Aggregate(int aid, String parameter, String operation){
        this.aid = aid;
        this.parameter = parameter;
        this.operation = operation;

    }

    /**
     * Gibt einen Parameter zurueck
     * @return  Einen Parameter des Aggregats
     */
    public String getParameter(){
        return parameter;
    }

    /**
     * Gibt eine UnterID zurueck
     * @return Eine Zahl als UnterID
     */
    public int getSubAid() {
        return subAid;
    }

    /**
     * Gibt einen Operator zurueck
     * @return  Einen Operator des Aggregates
     */
    public String getOperation(){
        return operation;
    }

    /**
     * Gibt die ID eines Aggregats zurueck
     * @return  Eine Zahl (Eine ID eines Aggregates)
     */
    public int getAid(){
        return aid;
    }

    /**
     * ToString-Methode
     * @return Gibt einen String mit AggregatID, Parameter und Operation
     */
    public String toString(){
        String s = "Aid:"+ aid + "Parameter:"+ parameter + "Operation:" + operation;
        return s;
    }
}
