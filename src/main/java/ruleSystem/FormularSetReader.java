package ruleSystem;

import databaseUtils.DatabaseConnector;

import java.util.ArrayList;
import java.util.Stack;

/**
 *
 * Created by Kathrin Schoepp on 10.09.2017
 **/
public class FormularSetReader {

    /**
     * Nimmt eine Liste an RegelIDs aus der Datenbank, die alle den Sensor sid benutzen
     * @param sid Die ID des Sensors der benutzt wird
     * @return  Gibt einen Stack mit allen IDs zureuck
     * @throws Exception
     */
   public static Stack<Integer> getRuleIDs(int sid) throws Exception {

      Stack<Integer> ridStack;
      DatabaseConnector databaseConnector = new DatabaseConnector();
      databaseConnector.connect();
      ridStack = databaseConnector.getRID(sid);
      databaseConnector.close();
      if(ridStack == null){
          throw new NullPointerException();
      }
      return ridStack;
   }


    /**
     * Gibt anhand einer SensorID alle zu dieser ID zugehoerigen Regeln und Aggregate zurueck
     * @param sid Die ID eines Sensors
     * @return Gibt ein Formular Objekt zurueck das eine Liste an Aggregaten und Regeln beinhaltet
     * @throws Exception
     */
   public static Formular getFormular(int sid) throws Exception {
       Formular formular = new Formular();
       Stack<Integer> ridStack = getRuleIDs(sid);
       ArrayList<Integer> ridList = new ArrayList<>();
       RuleSet ruleSet;
       RuleSet addingRuleSet;
       AggregateSet aggregateSet;
       DatabaseConnector databaseConnector = new DatabaseConnector();
       databaseConnector.connect();
       ruleSet = databaseConnector.getRuleSet(ridStack);
       databaseConnector.close();
       if (ruleSet == null) {
           throw new NullPointerException();
       }
       for (int j = 0; j < ruleSet.getSize(); j++) {
           if(!ridList.contains(ruleSet.getRid(j))) {
               int ruleID = ruleSet.getRid(j);
               ridList.add(ruleID);
           }
       }
       for (int i = 0; i < ruleSet.getSize(); i++) {
           String parameter = ruleSet.getParameter(i);
           if (parameter.charAt(0) == 'R') {
               String rID = ruleSet.getParameter(i).substring(1);
               int rid = Integer.parseInt(rID);
               if (parameter == null) {
                   throw new NullPointerException();
               }
               if (parameter.charAt(0) == 'R' && (!ridList.contains(rid))) {
                   ridList.add(rid);
                   databaseConnector.connect();
                   addingRuleSet = databaseConnector.getRule(parameter);
                   databaseConnector.close();
                   if (addingRuleSet == null) {
                       throw new NullPointerException();
                   }
                   for (int j = 0; j < addingRuleSet.getSize(); j++) {
                       int ruleid = Integer.parseInt(parameter.substring(1));
                       String addingParameter = addingRuleSet.getParameter(j);
                       String addingOperation = addingRuleSet.getOperation(j);
                       if (addingParameter == null) {
                           throw new NullPointerException();
                       }
                       if (addingOperation == null) {
                           throw new NullPointerException();
                       }
                       ruleSet.add(ruleid, addingParameter, addingOperation);

                   }

               }

           }

           aggregateSet = getAggregateSet(ruleSet);
           for(int l = 0; l < aggregateSet.getSize(); l++){
               if(aggregateSet.getParameter(l).charAt(0) == 'A'){
                   String aID = aggregateSet.getParameter(l).substring(1);
                   int aid = Integer.parseInt(aID);
                   databaseConnector.connect();
                   AggregateSet addingAggregateSet = databaseConnector.getAggregateByID(aid);
                   databaseConnector.close();
                   for( int m = 0; m < addingAggregateSet.getSize(); m++) {
                       aggregateSet.add(addingAggregateSet.getAid(m), addingAggregateSet.getParameter(m), addingAggregateSet.getOperation(m));
                   }
               }
           }
           formular.addAggregateSet(aggregateSet);
           formular.addRuleSet(ruleSet);


       }
       return formular;
   }

    /**
     * Gibt eine Liste an Aggregaten zurueck
     * @param ruleSet Liste der Regeln in der steht welche Aggregate aus der Datenbank geholt werden muessen
     * @return Die Liste der benoetigten Aggregate
     * @throws Exception
     */
   public static AggregateSet getAggregateSet(RuleSet ruleSet) throws Exception{
       DatabaseConnector databaseConnector = new DatabaseConnector();
       ArrayList<Integer> aidList = new ArrayList<>();
       AggregateSet aggregateSet = new AggregateSet();
       int x = 0;
       int j;
       for(int i = 0; i < ruleSet.getSize(); i++){
           String parameter = ruleSet.getParameter(i);
           if(parameter == null){
               throw new NullPointerException();
           }
           if (parameter.charAt(0) == 'A') {
               databaseConnector.connect();
               AggregateSet addingAggregateSet = databaseConnector.getAggregate(parameter);
               databaseConnector.close();
               if (aggregateSet == null) {
                   throw new NullPointerException();
               }
            for( j = x; j < addingAggregateSet.getSize(); j++){
                   int aid = addingAggregateSet.getAid(j);
                   if(!aidList.contains(aid)) {
                       for( x = j; x < addingAggregateSet.getSize(); x++) {
                           int addingAid = addingAggregateSet.getAid(x);
                           String addingParameter = addingAggregateSet.getParameter(x);
                           String addingOperation = addingAggregateSet.getOperation(x);
                           aggregateSet.add(addingAid, addingParameter, addingOperation);
                           aidList.add(aid);
                       }

                   }

            }
               j = 0;
               x = 0;

           }


       }
       return aggregateSet;
   }

}
