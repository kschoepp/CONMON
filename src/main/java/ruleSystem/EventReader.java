package ruleSystem;

import databaseUtils.DatabaseConnector;

/**
 *
 * Created by Kathrin Schoepp on 08.09.2017
 **/
public class EventReader {

    /**
     * Gibt Anhand der Liste von Regelergebnissen die Events in Form einer Liste zurueck
     * @param ruleResultList    Die Liste mit den Ergebnissen der Regeln
     * @return  Gibt eine Liste mit den zu triggernden Events zurueck
     * @throws Exception
     */
    public static EventList executeEvents(RuleResultList ruleResultList) throws Exception {
        EventList eventList;
        DatabaseConnector databaseConnector = new DatabaseConnector();
        databaseConnector.connect();
        eventList = databaseConnector.getEventList(ruleResultList);
        databaseConnector.close();
        if(eventList == null){
            throw new NullPointerException();
        }
        return eventList;
    }
}
