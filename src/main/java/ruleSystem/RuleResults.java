package ruleSystem;

import ruleSystem.expressionTree.ETreeStarter;

import java.lang.reflect.Parameter;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

/**
 *
 * Created by Kathrin Schoepp on 10.09.2017
 **/

public class RuleResults {


    /**
     * Berechnet die Ergebnis einer mitgegebenen RegelListe und AggregateListe
     *
     * @param formular Das Objekt welches die RegelListe und die AggregatListe beinhaltet
     * @param ridStack Stacck mit den benoetigten RegelIDs
     * @return Eine Liste mit den Ergebnissen der Regeln
     */
    public static RuleResultList executeRuleSet(Formular formular, Stack<Integer> ridStack, int sid) {
        RuleResultList ruleResultList = new RuleResultList();
        Queue<String> ruleQueue = new LinkedList<>();
        RuleSet ruleSet = formular.getRuleSet();
        AggregateSet aggregateSet = formular.getAggregteSet();
        if (ruleSet == null) {
            throw new NoRuleForSensorException("For this Sensor: "+sid+" does no Rule exist");
        }

        while (!ridStack.isEmpty()) {
            int rid = ridStack.pop();
                ruleQueue = getRuleQueue(ruleSet, aggregateSet, rid, ruleQueue);
                System.out.println(ruleQueue.toString());
                if (ruleQueue == null) {
                    throw new NullPointerException();
                }

                boolean result = (boolean) ETreeStarter.startETree(ruleQueue);
                ruleResultList.add(rid, result);
                ruleQueue.clear();

        }
        if (ruleResultList == null) {
            throw new NullPointerException();
        }
        return ruleResultList;

    }

    /**
     * Liefert eine Queue in der die Regel fertig aufgeschlüsselt ist, also in der alle mit R'n' angegebenen Regeln in Sensorvariablen da stehen
     *
     * @param ruleSet   Die Liste der Regel die aufgeschlüsselt werden
     * @param aggregateSet Die Liste der Aggregate mit denen aufgeschlüsselt wird
     * @param ruleId    Die ID der Regel die aufgeschlüsselt wird
     * @param ruleQueue Hilfs Parameter damit die Queue nicht ueberschrieben wird
     * @return Eine Queue, in der die Regel nur noch aus Sensorvariablen besteht
     */
    public static Queue<String> getRuleQueue(RuleSet ruleSet, AggregateSet aggregateSet, int ruleId, Queue<String> ruleQueue) {

        if (ruleSet == null) {
            throw new NullPointerException();
        }
        if (ruleQueue == null) {
            throw new NullPointerException();
        }
        for (int j = 0; j < ruleSet.getSize(); j++) {
            int x = j;
            while (x < ruleSet.getSize() && ruleSet.getRid(x) == ruleId) {

                String parameter = ruleSet.getParameter(x);
                if (parameter.charAt(0) == 'R') {
                    String s = parameter.substring(1);
                    int ruleID = Integer.parseInt(s);
                    ruleQueue = getRuleQueue(ruleSet, aggregateSet, ruleID, ruleQueue);
                    if (!ruleSet.getOperation(x).equals("#")) {
                        ruleQueue.add(ruleSet.getOperation(x));
                    }

                } else if (parameter.charAt(0)=='A'){
                    String aggregateParameter = ruleSet.getParameter(x);
                    String aggregateID = aggregateParameter.substring(1);
                    int aid = Integer.parseInt(aggregateID);
                    ruleQueue = getAggregate(aggregateSet,aid, ruleQueue);
                    if (!ruleSet.getOperation(x).equals("#")) {
                        ruleQueue.add(ruleSet.getOperation(x));
                    }




                } else if (ruleSet.getOperation(x).equals("#")) {
                    ruleQueue.add(ruleSet.getParameter(x));
                    break;
                }else{
                    ruleQueue.add(ruleSet.getParameter(x));
                    ruleQueue.add(ruleSet.getOperation(x));
                }

            x++;
            }
            j = x;

        }
        if (ruleSet == null) {
            throw new NullPointerException();
        }
        return ruleQueue;
    }

    public static Queue<String> getAggregate(AggregateSet aggregateSet, int aggregateid, Queue<String> ruleQueue){
        String aggregateParameter = "";
        int i = 0;
        for (i = 0; i < aggregateSet.getSize(); i++) {
            if (aggregateSet.getAid(i) == aggregateid) {
                aggregateParameter = aggregateSet.getParameter(i);
                break;
            }
        }
            if (aggregateParameter.charAt(0) == 'A') {
                String agid = aggregateParameter.substring(1);
                int aid = Integer.parseInt(agid);
                getAggregate(aggregateSet, aid, ruleQueue);


            } else {
                for ( int j = 0; j < aggregateSet.getSize(); j++){
                    if (aggregateSet.getAid(j) == aggregateid) {
                        ruleQueue.add(aggregateSet.getParameter(j));
                        if (!aggregateSet.getOperation(j).equals("#")) {
                            ruleQueue.add(aggregateSet.getOperation(j));
                        }

                    }
                }



            }






        return ruleQueue;
    }
}
