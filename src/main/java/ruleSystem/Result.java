package ruleSystem;

/**
 *
 * Created by Kathrin Schoepp on 08.09.2017
 **/
public class Result {
    private int rid;
    private boolean result;

    /**
     * Konstruktor fuer ein Ergebniss
     * @param rid   Die ID einer Regel
     * @param result    Das Ergebnis einer Regel
     */
    public Result(int rid, boolean result){
        this.rid = rid;
        this.result = result;

    }

    /**
     * Gibt die ID zurueck
     * @return Eine Zahl(die ID)
     */
    public int getID(){
        return rid;
    }

    /**
     * Gibt das Ergebnis zurueck
     * @return  Einen boolean Wert (das Ergebnis)
     */
    public boolean getResult(){
        return result;
    }
}
