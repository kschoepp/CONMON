package ruleSystem;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * Created by Kathrin Schoepp on 08.09.2017
 **/
public class RuleSet {

    List<Rule> rules = new ArrayList<Rule>();

    /**
     * Konstruktor fuer eine Liste an Regeln
     * @param rid   Die ID einer Regel
     * @param parameter Ein Parameter einer Regel
     * @param operation Ein Operator einer Regel
     */
    public RuleSet(int rid, String parameter, String operation){

        rules.add(new Rule(rid, parameter, operation));
    }

    /**
     * Konstruktor fuer eine Liste von Regeln ohne Parameter
     */
    public RuleSet(){

    }

    /**
     * Hinzufuegen eines RegelObjektes mit RegelID, Parameter und Operator
     * @param rid Die ID einer Regel
     * @param parameter Ein Parameter einer Regel
     * @param operation Ein Operator einer Regel
     */
    public void add(int rid, String parameter, String operation){

        rules.add(new Rule(rid, parameter, operation));
    }

    /**
     * Hinzufuegen mehrerer RegelObjekte aus einem RuleSet
     * @param ruleSet Das RuleSet in dem die Teilregeln stehen
     */
    public void addByRuleSet(RuleSet ruleSet){
        for (int i = 0; i < ruleSet.getSize(); i++) {
            rules.add(new Rule(ruleSet.getRid(i), ruleSet.getParameter(i), ruleSet.getOperation(i)));
        }
    }

    /**
     * Gibt einen Parameter einer Regel zurueck
     * @param index Die Stelle in der Liste, an der sich das Objekt in der Liste befindet, von dem das Parameter zurueck gegeben wird
     * @return  Einen Parameter einer Regel
     */
    public String getParameter(int index){
        String parameter = rules.get(index).getParameter();
        return parameter;
    }

    /**
     * Gibt einen Operator einer Regel zurueck
     * @param index Die Stelle in der Liste, an der sich das Objekt befindet, von dem der Operator zurueck gegeben wird
     * @return  Ein Operator einer Regel
     */
    public String getOperation(int index){
        String operation = rules.get(index).getOperation();
        return operation;
    }

    /**
     * Gibt die ID einer Regel zurueck
     * @param index Die Stelle in der List, an der sich das Objekt befindet, von dem die ID zurueck gegeben wird
     * @return Eine Zahl (RegelID)
     */
    public int  getRid(int index){
        int rid = rules.get(index).getRid();
        return rid;
    }

    /**
     * Gibt eien RegelObjekt zurueck
     * @param index Die Stelle in der Liste, an der sich das Objekt befindet, welches zureuck gegeben wird
     * @return Ein Regelobjekt
     */
    public Rule getRuleObject(int index){
        Rule rule = rules.get(index);
        return rule;
    }

    /**
     * Gibt die Groesse der Liste zureuck
     * @return  Eine Zahl (Die Groesse der Liste)
     */
    public int getSize() {
        return rules.size();
    }
}

