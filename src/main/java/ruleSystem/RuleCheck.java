package ruleSystem;

import java.util.Stack;

import static ruleSystem.EventReader.executeEvents;
import static ruleSystem.RuleResults.executeRuleSet;
import static ruleSystem.FormularSetReader.getRuleIDs;
import static ruleSystem.FormularSetReader.getFormular;

/**
 *
 * Created by Kathrin Schoepp on 08.09.2017
 **/
public class RuleCheck {

    /**
     * Methode die vom EventSystem aufgerufen wird um an Hand der SensorID eine Liste mit Events zurueck gibt
     * @param sid Die ID eines Sensors
     * @return Gibt eine Liste an Events zurueck
     * @throws Exception
     */

    public static EventList startRuleCheck(int sid) throws Exception {

        Stack<Integer> ridStack = getRuleIDs(sid);
        Formular formular = getFormular(sid);
        if(formular == null){
            throw new NullPointerException();
        }
        RuleResultList ruleResults = executeRuleSet(formular,ridStack,sid);
        if(ruleResults == null){
            throw new NullPointerException();
        }
        EventList eventList = executeEvents(ruleResults);
        if(eventList == null){
            throw new NullPointerException();
        }
        return eventList;
    }
}
