package ruleSystem;

/**
 *
 * Created by Kathrin Schoepp on 08.09.2017
 **/
public class Event {

    private String eventTyp;
    private String message;


    /**
     * Konstruktor fuer ein Event
     * @param eventTyp  Gibt an wo das Event getriggert werden soll
     * @param message   Die Nachricht die bei einem Event ausgegeben wird
     */
    public Event(String eventTyp, String message){
        this.eventTyp = eventTyp;
        this.message = message;

    }

    /**
     * Gibt eine Nachricht für das Event zurueck
     * @return Einen String
     */
    public String getMessage(){
        return message;
    }

    /**
     * Gibt einen Eventtyp zurueck
     * @return Einen String
     */
    public String getEventTyp(){
        return eventTyp;
    }

    /**
     * ToString() für ein Event
     * @return  Gibt ein Event als String zurueck
     */
    public String toString(){
        String s = "Eventtyp: "+ eventTyp +"\nMessage: "+ message;
        return s;
    }
}
