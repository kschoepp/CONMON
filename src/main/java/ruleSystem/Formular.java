package ruleSystem;

/**
 *
 * Created by Kathrin Schoepp on 10.09.2017
 **/

public class Formular {
    private RuleSet ruleSet;
    private AggregateSet aggretateSet;

    /**
     * Konstruktor ohne Parameter
     */
    public Formular(){

    }

    /**
     * Fuegt ein RuleSet ein
     * @param rs Ein RuleSet bestehend aus Regeln
     */
    public void addRuleSet(RuleSet rs){
        this.ruleSet = rs;
    }

    /**
     * Fuegt ein AggregateSet ein
     * @param as Ein AggregateSet bestehend aus Aggregaten
     */
    public void addAggregateSet(AggregateSet as){
        this.aggretateSet = as;
    }

    /**
     * Gibt ein RuleSet zurueck
     * @return Ein RuleSet
     */
    public RuleSet getRuleSet(){
        return ruleSet;
    }

    /**
     * Gibt ein AggregateSet zurueck
     * @return Ein AggregateSet
     */
    public AggregateSet getAggregteSet(){
        return aggretateSet;
    }
}
