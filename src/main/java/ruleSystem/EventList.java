package ruleSystem;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * Created by Kathrin Schoepp on 08.09.2017
 **/
public class EventList {

    List<Event> eventList = new ArrayList<Event>();

    /**
     * Konstruktor fuer die Eventliste
     * @param eventType Gibt an wo bzw. wie ein Event getriggert werden soll
     * @param message   Die Nachricht, die bei einem Event ausgegeben wird
     */
    public EventList(String eventType, String message){

        eventList.add(new Event(eventType, message));
    }

    /**
     * Konstruktor fuer die Eventliste ohne Parameter
     */
    public EventList(){

    }

    /**
     * Einfuegen in die Eventliste
     * @param eventType Gibt an wo bzw. wie ein Event getriggert werden soll
     * @param message   Die Nachricht, die bei einem Event ausgegeben wird
     */
    public void add(String eventType, String message){

        eventList.add(new Event(eventType, message));
    }

    /**
     * Gibt ein EventListObjekt zurueck
     * @param index Die Stelle in einer Eventliste die zurueckgegeben wird
     * @return Ein Event bestehend aud Eventtype und Message
     */
    public Event getEventListObject(int index){
        Event event = eventList.get(index);
        return event;
    }

    /**
     * Gibt die Groesse der Eventliste an
     * @return  Eine Zahl( die Groesse der Eventliste)
     */
    public int getSize(){
        return eventList.size();
    }
}
