import databaseUtils.DatabaseConnector;

import java.util.Stack;

/**
 * Created by kathrin on 05.09.2017.
 */
public class GetRIDTest {

    private static int sid = 1;
    private static Stack<Integer> ridStack;

    public static void main(String[] args) throws Exception {
        DatabaseConnector dc = new DatabaseConnector();
        dc.connect();
        ridStack = dc.getRID(sid);
        dc.close();
        for(int i = 0; i < ridStack.size(); i++ ) {
            System.out.println(ridStack.toString());
        }
    }
}
