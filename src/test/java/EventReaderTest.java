import ruleSystem.EventList;
import ruleSystem.EventReader;
import ruleSystem.RuleResultList;

/**
 * Created by kathrin on 06.09.2017.
 */
public class EventReaderTest {
    private static RuleResultList rrs = new RuleResultList();
    private static EventList el;

    public EventReaderTest(){
        rrs.add(1, false);
        rrs.add(2, true);
        rrs.add(3, false);
        rrs.add(4, true);
        rrs.add(5, false);
        rrs.add(305, false);
    }

    public static void main(String[] args) throws Exception {
        EventReaderTest ert = new EventReaderTest();
        EventReader er =  new EventReader();
        el = er.executeEvents(rrs);
        for(int i = 0; i < el.getSize(); i++){
            System.out.println(el.getEventListObject(i).toString());
        }
    }
}
