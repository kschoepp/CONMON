import ruleSystem.AggregateSet;
import ruleSystem.RuleResults;
import ruleSystem.RuleSet;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

/**
 * Created by kathrin on 05.09.2017.
 */
public class GetRuleQueueTest {

    private static RuleSet rs = new RuleSet();
    private static AggregateSet as = new AggregateSet();


    public GetRuleQueueTest() {
        rs.add(1, "A1", "+");
        rs.add(1, "s1", "&&");
        rs.add(1, "A1", "#");
        as.add(1, "s2", "<");
        as.add(1, "s3", "#");

    }

    public static void main(String[] args) {
        Queue<String> ruleQueue = new LinkedList<>();
        GetRuleQueueTest grqt = new GetRuleQueueTest();
        RuleResults rr = new RuleResults();
        ruleQueue = rr.getRuleQueue(rs,as,1,ruleQueue );
        System.out.print(ruleQueue.toString());
    }
}
