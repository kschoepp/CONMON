import databaseUtils.DatabaseConnector;
import ruleSystem.RuleSet;

import java.util.Stack;

/**
 * Created by kathrin on 06.09.2017.
 */
public class GetRuleSetTest {

    private static int sid = 2005;
    private static RuleSet rs;
    private static Stack<Integer> ridStack = new Stack<>() ;

    public GetRuleSetTest(){
        ridStack.push(1);
        ridStack.push(2);
        ridStack.push(3);
        ridStack.push(4);
        ridStack.push(5);
        ridStack.push(6);
    }
    public static void main(String[] args) throws Exception {
        GetRuleSetTest grst = new GetRuleSetTest();
        DatabaseConnector dc = new DatabaseConnector();
        dc.connect();
        rs = dc.getRuleSet(ridStack);
        dc.close();
        for(int i = 0; i < rs.getSize(); i++) {
            System.out.println(rs.getRuleObject(i).toString());
        }
    }
}
