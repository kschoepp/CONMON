import databaseUtils.DatabaseConnector;
import ruleSystem.EventList;
import ruleSystem.RuleResultList;

/**
 * Created by kathrin on 06.09.2017.
 */
public class GetEventListTest {

    private static RuleResultList rrs = new RuleResultList();
    private static EventList el;

    public GetEventListTest(){
        rrs.add(1,false);
        rrs.add(2,true);
        rrs.add(3,false);
        rrs.add(4,true);
        rrs.add(305,false);
    }

    public static void main(String[] args) throws Exception {
        GetEventListTest gelt = new GetEventListTest();
        DatabaseConnector dc = new DatabaseConnector();
        dc.connect();
        el = dc.getEventList(rrs);
        dc.close();
        for(int i = 0; i < el.getSize(); i++) {
            System.out.println(el.getEventListObject(i).toString());
        }
    }
}
