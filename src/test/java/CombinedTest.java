import ruleSystem.*;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

import static ruleSystem.FormularSetReader.getFormular;
import static ruleSystem.FormularSetReader.getRuleIDs;

/**
 * Created by kathrin on 08.09.2017.
 */
public class CombinedTest {

    private static int sid = 2003;
    private static RuleResultList rrl = new RuleResultList();
    private static Stack<Integer> ridStack = new Stack<>();

    public static void main(String[] args) throws Exception {
        RuleResults rr = new RuleResults();

        Formular formular = FormularSetReader.getFormular(sid);
        ridStack = FormularSetReader.getRuleIDs(sid);
        rrl = rr.executeRuleSet(formular, ridStack, sid);
        for(int i = 0; i < rrl.getSize(); i++){
            System.out.println(rrl.getResult(i));
        }
    }
}
