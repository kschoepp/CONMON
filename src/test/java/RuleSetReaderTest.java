import ruleSystem.AggregateSet;
import ruleSystem.Formular;
import ruleSystem.RuleSet;
import ruleSystem.FormularSetReader;

/**
 * Created by kathrin on 07.09.2017.
 */
public class RuleSetReaderTest {
    private static int sid = 2005;
    private static Formular formular;
    private static  RuleSet rs;
    private static AggregateSet as;

    public static void main(String[] args) throws Exception{
        FormularSetReader rsr = new FormularSetReader();
        formular = rsr.getFormular(sid);
        rs = formular.getRuleSet();
        as = formular.getAggregteSet();
        for(int i = 0; i < rs.getSize(); i++){
            System.out.println(rs.getRuleObject(i).toString());
        }
        for(int j = 0; j < as.getSize(); j++){
            System.out.println(as.getAggregateObject(j).toString());
        }
    }

}
