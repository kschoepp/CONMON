import ruleSystem.*;

import java.util.Stack;

/**
 * Created by kathrin on 07.09.2017.
 */
public class ExecuteRuleSetTest {
    private static RuleSet rs = new RuleSet();
    private static AggregateSet as = new AggregateSet();
    private static Stack<Integer> ridStack = new Stack<>();
    private static RuleResultList rrl;
    private static Formular formular = new Formular();
    private static int sid;

    public ExecuteRuleSetTest(){
        //as.add(1,"s1","+");
        //as.add(1,"s2","#");
        //as.add(2,"s3","-");
        //as.add(2,"s4","#");
        //rs.add(1,"A1","<");
        //rs.add(1,"A2","#");
        //rs.add(2,"R1","&&");
        //rs.add(2,"s3","<");
        //rs.add(2,"s4","#");
        rs.add(1,"avg2005(50)","<");
        rs.add(1,"c50","#");
        ridStack.push(1);
        //ridStack.push(2);

        formular.addRuleSet(rs);
        formular.addAggregateSet(as);


    }

    public static void main(String[] args) {
        ExecuteRuleSetTest erst = new ExecuteRuleSetTest();
        RuleResults rr = new RuleResults();

        rrl = rr.executeRuleSet(formular,ridStack,sid);
        for(int i = 0; i < rrl.getSize(); i++){
            rrl.getResultObject(i).toString();
        }
    }
}
