import databaseUtils.DatabaseConnector;
import ruleSystem.RuleSet;

/**
 * Created by kathrin on 06.09.2017.
 */
public class GetRuleTest {

    private static String rule = "R1";
    private static RuleSet rs;

    public static void main(String[] args) throws Exception {
        DatabaseConnector dc = new DatabaseConnector();
        dc.connect();
        rs = dc.getRule(rule);
        dc.close();
        for(int i = 0; i < rs.getSize(); i++) {
            System.out.println( rs.getRuleObject(i).toString());
        }
    }
}
