# CONMON-backend
Das Backend dient zur Verwaltung der Daten und der Berechnung der Regeln.

Die Daten werden vom OPCServer an die Datenbank gesendet. Das Backend bekommt eine Datenänderung mit, zeigt den Wert im Frontend an und schaut sich anhand der SensorID an, ob es für diesen Sensor eine Regel gibt. Wenn es Regeln dafür gibt werden diese ausgerechnet. Anhand der RegelID wird geschaut ob es ein Event für diese Regel gibt. Wenn ja, wird das Ergebnis der Regel mit dem Triggervalue verglichen. Wenn diese gleich sind, wird ein Event geworfen.

## **JAR**

Da das Backend als Maven-Projekt stukturiert ist, kann man einfach per `mvn package` die JAR-Datei kompilieren.

### **Hinweis**:
Nach dem kompilieren kann unter Umständen ein Fehler auftreten, dass die Main nicht gefunden werden kann. Der Fehler konnte von unserer Seite aus leider (falls er auftrat) nicht behoben werden. Folgende Vorgehensweisen wurden erfolgslos versucht:
1. Manuelles überschreiben der Main-Funktion per Command-Line Parameter beim Start der JAR per `java -cp <filename.jar> <Package.Main.Method>`.
2. Definieren der Main-Methode im POM-File.
3. Manuelles nachtragen (bzw. abändern) der Main-Methode im MANIFEST.MF File.

Auch keine Kombination der oben stehenden Lösungsansätze war erfolgreich.

## **Datenbankschema**

Das Datenbankschema unter dem SQL-Ordner in der timescaleSetup.sql Datei.


### **Machine**

Eine Datenbanktabelle in der eine Maschine verwaltet wird.
Sie beinhaltet:

 - eine MaschinenID (mid)
 - eine Adresse zum OPCServer einer Maschine (uri)
 - einen Maschinenname (mname)
 - einen Status ob eine Maschine aktiv ist (active)
 - ein Zeitstempel, wann eine Maschine das erste Mal aktiv war (created)
 - der Ort an dem die Maschine steht (position)
 - eine Beschreibung der Maschine (description)
 - einen Status ob die Maschine verändert wurde (changed)
 
 
**uri**:
Dies ist die Adresse, mit der man den OPCServer einer Maschine erreichen kann, z.B. opc.htwsaar.de:4840.

**active**:
Dieser Status gibt an, ob die Daten einer Maschine erfasst werden können und ob diese dem entsprechend im Frontend angezeigt werden.

**created**:
Dieser Zeitstempel gibt an, wann eine Maschine das erste Mal in Betrieb war.

**changed**:
Wenn eine Maschine verändert wurde wird dieser Status auf _true_ gesetzt. Wenn die Änderungen übernommen wurden, wird der Status wieder auf _false_ gesetzt.


### **Sensor**
Eine Datenbank Tabelle in der ein Sensor verwaltet wird.
Sie beinhaltet:

 - eine SensorID (sid)
 - eine ID der Maschine, zu der dieser Sensor gehört (mid)
 - der Name dieser Maschine (mname)
 - einen Namespace zur Adressierung (namespace)
 - eine NodeID zur Adressierung (nodeid)
 - einen Sensornamen (sname)
 - einen Typ der Werte (valuetype)
 - einen Wert um den sich die Werte mindestens ändern müssen (valuemindelta)
 - eine Einheit der Werte (z.B. °C) (unit)
 - einen Status ob ein Sensor aktiv ist (aktive)
 - einen Zeitstempel wann die Maschine das erste Mal aktiv war (created)
 - target; ungenutzt (immer leerer String)
 - einen Status ob der Sensor verändert wurde (changed)

**namespace & nodeid**:
Der Namespace und die NodeID geben zusammen die Adresse eines Sensors auf dem OPCServer an. Diese Adresse ist eindeutig identifizierbar.

**valuetype**:
Dies gibt an, welche Art von Datentyp der Wert ist. (z.B. int, double)

**valuemindelta**:
Dieser Wert gibt an, um welchen Faktor sich die empfangenen Daten ändern müssen, um gespeichert zu werden.

**active**:
 Dieser Status gibt an, ob die Daten eines Sensors erfasst werden können und ob diese dem entsprechend im Frontend angezeigt werden.

**created**:
Dieser Zeitstempel gibt an, wann ein Sensor das erste Mal in Betrieb war.

**changed**:
Wenn ein Sensor verändert wurde wird dieser Status auf _true_ gesetzt. Wenn die Änderungen übernommen wurden, wird der Status wieder auf _false_ gesetzt.


### **MachineData**
Eine Datenbank Tabelle in der die Daten einer Maschine verwaltet werden.
Sie beinhaltet:

 - eine MaschinenID (mid)
 - einen Status ob die Maschine verbunden ist (connected)
 - einen Zeitstempel für den Status (timestamp)

**connected**:
Dieser Status ist _true_, wenn die Maschine verbunden ist. Ansonsten ist der Status _false_.

**timestamp**:
Dieser Zeitstempel gibt an, wann der Status zugetroffen hat.


### **SensorData**
Eine Datenbank Tabelle in der die Daten eines Sensors verwaltet werden.
Sie beinhaltet:

 - eine SensorID (sid)
 - einen Wert (value)
 - einen Zeitstempel des Wertes (timestamp)
 
**timestamp**:
Dieser Zeitstempel gibt an, wann der Wert erreicht wurde.


### **DataTarget**
Tabelle muss existieren, ist aber leer. (nicht genutzt)


### **Rule**
Eine Datenbank Tabelle in der die Regeln verwaltet werden.
Sie beinhaltet:

 - eine RegelID (rid)
 - eine UnterregelID (subrid)
 - einen Regelnamen (rname)
 - einen Parameter (parameter)
 - eine Operation (operation)
 - die benutzen Sensoren (sensorused)
 
**Aufbau einer Regel**:
Eine Regel ist in der Datenbank über mehrere Zeilen aufgeteilt. Deshalb existieren zusätzlich zu der RegelID noch mehrere UnterregelIDs. In jeder Zeile ist nur ein Parameter und ein Operator enhalten. Die letzte Zeile enhält als Operator ein # um zu kennzeichenen, dass die Regel dort beendet ist. Alle Zeilen dieser Regel haben die selbe RegelID. Die UnterregelIDs verlaufen fortlaufend bei einer Regel und fangen bei der nächsten wieder neu an.

**parameter**:
Ein Parameter kann eine Regel, ein Aggregat, ein MinWert (z.B. min2005(50)), ein MaxWert (z.B. max2005(50)) , eine AvgWert (z.B. avg2005(50)), ein MedWert (z.B. med2005(50)) oder eine Konstante sein.

**operation**:
Eine Operation kann >, <, >=, <=, ==, !=, || und &&.


### **Aggregate**
Eine Datenbank Tabelle in der die Aggregate verwaltet werden.
Sie beinhaltet:

 - eine AggregatID (aid)
 - eine UnteraggregatID (subaid)
 - einen Aggregatnamen (aname)
 - einen Parameter (parameter)
 - eine Operation (operation)
 - die benutzen Sensoren (sensorused)
 
**Aufbau eines Aggregats**:
Eine Aggregat ist in der Datenbank über mehrere Zeilen aufgeteilt. Deshalb existieren zusätzlich zu der AggregatID noch mehrere UnteraggregatIDs. In jeder Zeile ist nur ein Parameter und ein Operator enhalten. Die letzte Zeile enhält als Operator ein # um zu kennzeichenen, dass das Aggregat dort beendet ist. Alle Zeilen dieses Aggregats haben die selbe AggregatID. Die UnteraggregatIDs verlaufen fortlaufend bei eine Aggregats und fangen bei der nächsten wieder neu an.

**parameter**:
Ein Parameter kann ein Aggregat, ein MinWert (z.B. min2005(50)), ein MaxWert (z.B. max2005(50)) , eine AvgWert (z.B. avg2005(50)), ein MedWert (z.B. med2005(50)) oder eine Konstante sein.

**operation**:
Eine Operation kann +, -, * und /.

### **Event**
Eine Datenbank Tabelle in der die Events verwaltet werden.
Sie beinhaltet:

 - eine EventID (eventid)
 - eine RegelID (rid)
 - ein Eventtyp (eventtype)
 - ein Nachrichtentext (message)
 - ein Wert bei dem das Event ausgelöst wird (triggervalue)

**eventtype**:
Dies gibt an, welches Event ausgelöt werden soll. In unserem Programm ist nur die Mail programmiert, deshalb muss dort immer "mail" drin stehen.

**message**:
Dieser Nachrichtentext wird in der Email versendet.

**triggervalue**:
Dies ist der Wert, den eine Regel annimmt, bei dem dieses Event ausgelöst werden soll.